<?php

namespace Database\Factories;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\ScheduleAvailability>
 */
class ScheduleAvailabilityFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'start' => Carbon::now()->toDateTimeString(),
            'end' => Carbon::now()->addDays(rand(1, 10))->toDateTimeString(),
            'is_recurring' => rand(0, 1),
            'user_id' => User::factory()->create()->id,
        ];
    }
}
