<?php

namespace Database\Seeders;

use App\Models\ScheduleAvailability;
use Illuminate\Database\Seeder;

class ScheduleAvailabilitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=0; $i<20; $i++) {
            ScheduleAvailability::factory()->create();
        }
    }
}
