<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>Availability Management</title>

    <link href="https://fonts.googleapis.com/css?family=PT+Sans&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="{{ asset("vendor/scribe/css/theme-default.style.css") }}" media="screen">
    <link rel="stylesheet" href="{{ asset("vendor/scribe/css/theme-default.print.css") }}" media="print">

    <script src="https://cdn.jsdelivr.net/npm/lodash@4.17.10/lodash.min.js"></script>

    <link rel="stylesheet"
          href="https://unpkg.com/@highlightjs/cdn-assets@10.7.2/styles/obsidian.min.css">
    <script src="https://unpkg.com/@highlightjs/cdn-assets@10.7.2/highlight.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jets/0.14.1/jets.min.js"></script>

    <style id="language-style">
        /* starts out as display none and is replaced with js later  */
                    body .content .bash-example code { display: none; }
                    body .content .javascript-example code { display: none; }
            </style>

    <script>
        var baseUrl = "http://localhost";
        var useCsrf = Boolean();
        var csrfUrl = "/sanctum/csrf-cookie";
    </script>
    <script src="{{ asset("vendor/scribe/js/tryitout-3.26.0.js") }}"></script>

    <script src="{{ asset("vendor/scribe/js/theme-default-3.26.0.js") }}"></script>

</head>

<body data-languages="[&quot;bash&quot;,&quot;javascript&quot;]">

<a href="#" id="nav-button">
    <span>
        MENU
        <img src="{{ asset("vendor/scribe/images/navbar.png") }}" alt="navbar-image" />
    </span>
</a>
<div class="tocify-wrapper">
    
            <div class="lang-selector">
                                            <button type="button" class="lang-button" data-language-name="bash">bash</button>
                                            <button type="button" class="lang-button" data-language-name="javascript">javascript</button>
                    </div>
    
    <div class="search">
        <input type="text" class="search" id="input-search" placeholder="Search">
    </div>

    <div id="toc">
                                                                            <ul id="tocify-header-0" class="tocify-header">
                    <li class="tocify-item level-1" data-unique="introduction">
                        <a href="#introduction">Introduction</a>
                    </li>
                                            
                                                                    </ul>
                                                <ul id="tocify-header-1" class="tocify-header">
                    <li class="tocify-item level-1" data-unique="authenticating-requests">
                        <a href="#authenticating-requests">Authenticating requests</a>
                    </li>
                                            
                                                </ul>
                    
                    <ul id="tocify-header-2" class="tocify-header">
                <li class="tocify-item level-1" data-unique="endpoints">
                    <a href="#endpoints">Endpoints</a>
                </li>
                                    <ul id="tocify-subheader-endpoints" class="tocify-subheader">
                                                    <li class="tocify-item level-2" data-unique="endpoints-POSTapi-register">
                        <a href="#endpoints-POSTapi-register">POST api/register</a>
                    </li>
                                    <li class="tocify-item level-2" data-unique="endpoints-POSTapi-login">
                        <a href="#endpoints-POSTapi-login">POST api/login</a>
                    </li>
                                    <li class="tocify-item level-2" data-unique="endpoints-DELETEapi-logout">
                        <a href="#endpoints-DELETEapi-logout">DELETE api/logout</a>
                    </li>
                                    <li class="tocify-item level-2" data-unique="endpoints-GETapi-email-verify--id---hash-">
                        <a href="#endpoints-GETapi-email-verify--id---hash-">GET api/email/verify/{id}/{hash}</a>
                    </li>
                                    <li class="tocify-item level-2" data-unique="endpoints-POSTapi-email-verification-notification">
                        <a href="#endpoints-POSTapi-email-verification-notification">POST api/email/verification-notification</a>
                    </li>
                                    <li class="tocify-item level-2" data-unique="endpoints-GETapi-availabilities">
                        <a href="#endpoints-GETapi-availabilities">Return all availabilities.</a>
                    </li>
                                    <li class="tocify-item level-2" data-unique="endpoints-POSTapi-availabilities">
                        <a href="#endpoints-POSTapi-availabilities">Store availability timeframe.</a>
                    </li>
                                    <li class="tocify-item level-2" data-unique="endpoints-GETapi-users--id-">
                        <a href="#endpoints-GETapi-users--id-">Display the specified resource.</a>
                    </li>
                                                    </ul>
                            </ul>
        
                        
            </div>

            <ul class="toc-footer" id="toc-footer">
                            <li><a href="{{ route("scribe.postman") }}">View Postman collection</a></li>
                            <li><a href="{{ route("scribe.openapi") }}">View OpenAPI spec</a></li>
                            <li><a href="http://github.com/knuckleswtf/scribe">Documentation powered by Scribe ✍</a></li>
                    </ul>
        <ul class="toc-footer" id="last-updated">
        <li>Last updated: April 6 2022</li>
    </ul>
</div>

<div class="page-wrapper">
    <div class="dark-box"></div>
    <div class="content">
        <h1 id="introduction">Introduction</h1>
<p>Simple Availability Management System</p>
<p>This documentation aims to provide all the information you need to work with our API.</p>
<aside>As you scroll, you'll see code examples for working with the API in different programming languages in the dark area to the right (or as part of the content on mobile).
You can switch the language used with the tabs at the top right (or from the nav menu at the top left on mobile).</aside>
<blockquote>
<p>Base URL</p>
</blockquote>
<pre><code class="language-yaml">availability-management.test</code></pre>

        <h1 id="authenticating-requests">Authenticating requests</h1>
<p>To authenticate requests, include an <strong><code>Authorization</code></strong> header with the value <strong><code>"Bearer {YOUR_AUTH_KEY}"</code></strong>.</p>
<p>All authenticated endpoints are marked with a <code>requires authentication</code> badge in the documentation below.</p>
<p>You can retrieve your token by visiting your dashboard and clicking <b>Generate API token</b>.</p>

        <h1 id="endpoints">Endpoints</h1>

    

            <h2 id="endpoints-POSTapi-register">POST api/register</h2>

<p>
</p>



<span id="example-requests-POSTapi-register">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request POST \
    "availability-management.test/api/register" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json" \
    --data "{
    \"name\": \"zifjnyagjdklbonlhvhvegdjkhrdihlyotymesbiwwtgucuaxaqqbiluootkxcyflmgjbbplnwbrvoryvpfchvtwwjsbxlkoxsvqdyptehxredfwtdaknkhngyqheieghmsdocouomtxbnomdkdvns\",
    \"email\": \"ystehr@example.com\",
    \"password\": \"ygi\",
    \"timezone\": \"architecto\"
}"
</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "availability-management.test/api/register"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "name": "zifjnyagjdklbonlhvhvegdjkhrdihlyotymesbiwwtgucuaxaqqbiluootkxcyflmgjbbplnwbrvoryvpfchvtwwjsbxlkoxsvqdyptehxredfwtdaknkhngyqheieghmsdocouomtxbnomdkdvns",
    "email": "ystehr@example.com",
    "password": "ygi",
    "timezone": "architecto"
};

fetch(url, {
    method: "POST",
    headers,
    body: JSON.stringify(body),
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-POSTapi-register">
</span>
<span id="execution-results-POSTapi-register" hidden>
    <blockquote>Received response<span
                id="execution-response-status-POSTapi-register"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-register"></code></pre>
</span>
<span id="execution-error-POSTapi-register" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-register"></code></pre>
</span>
<form id="form-POSTapi-register" data-method="POST"
      data-path="api/register"
      data-authed="0"
      data-hasfiles="0"
      data-isarraybody="0"
      data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}'
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('POSTapi-register', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-POSTapi-register"
                    onclick="tryItOut('POSTapi-register');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-POSTapi-register"
                    onclick="cancelTryOut('POSTapi-register');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-POSTapi-register" hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-black">POST</small>
            <b><code>api/register</code></b>
        </p>
                            <h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
        <p>
            <b><code>name</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
                <input type="text"
               name="name"
               data-endpoint="POSTapi-register"
               value="zifjnyagjdklbonlhvhvegdjkhrdihlyotymesbiwwtgucuaxaqqbiluootkxcyflmgjbbplnwbrvoryvpfchvtwwjsbxlkoxsvqdyptehxredfwtdaknkhngyqheieghmsdocouomtxbnomdkdvns"
               data-component="body" hidden>
    <br>
<p>Must not be greater than 255 characters.</p>
        </p>
                <p>
            <b><code>email</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
                <input type="text"
               name="email"
               data-endpoint="POSTapi-register"
               value="ystehr@example.com"
               data-component="body" hidden>
    <br>
<p>Must be a valid email address.</p>
        </p>
                <p>
            <b><code>password</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
                <input type="text"
               name="password"
               data-endpoint="POSTapi-register"
               value="ygi"
               data-component="body" hidden>
    <br>
<p>Must be at least 7 characters.</p>
        </p>
                <p>
            <b><code>timezone</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
                <input type="text"
               name="timezone"
               data-endpoint="POSTapi-register"
               value="architecto"
               data-component="body" hidden>
    <br>

        </p>
        </form>

            <h2 id="endpoints-POSTapi-login">POST api/login</h2>

<p>
</p>



<span id="example-requests-POSTapi-login">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request POST \
    "availability-management.test/api/login" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json" \
    --data "{
    \"email\": \"isidro13@example.org\",
    \"password\": \"z\"
}"
</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "availability-management.test/api/login"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "email": "isidro13@example.org",
    "password": "z"
};

fetch(url, {
    method: "POST",
    headers,
    body: JSON.stringify(body),
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-POSTapi-login">
</span>
<span id="execution-results-POSTapi-login" hidden>
    <blockquote>Received response<span
                id="execution-response-status-POSTapi-login"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-login"></code></pre>
</span>
<span id="execution-error-POSTapi-login" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-login"></code></pre>
</span>
<form id="form-POSTapi-login" data-method="POST"
      data-path="api/login"
      data-authed="0"
      data-hasfiles="0"
      data-isarraybody="0"
      data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}'
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('POSTapi-login', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-POSTapi-login"
                    onclick="tryItOut('POSTapi-login');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-POSTapi-login"
                    onclick="cancelTryOut('POSTapi-login');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-POSTapi-login" hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-black">POST</small>
            <b><code>api/login</code></b>
        </p>
                            <h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
        <p>
            <b><code>email</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
                <input type="text"
               name="email"
               data-endpoint="POSTapi-login"
               value="isidro13@example.org"
               data-component="body" hidden>
    <br>
<p>Must be a valid email address.</p>
        </p>
                <p>
            <b><code>password</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
                <input type="text"
               name="password"
               data-endpoint="POSTapi-login"
               value="z"
               data-component="body" hidden>
    <br>
<p>Must be at least 6 characters.</p>
        </p>
        </form>

            <h2 id="endpoints-DELETEapi-logout">DELETE api/logout</h2>

<p>
</p>



<span id="example-requests-DELETEapi-logout">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request DELETE \
    "availability-management.test/api/logout" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json"</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "availability-management.test/api/logout"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers,
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-DELETEapi-logout">
</span>
<span id="execution-results-DELETEapi-logout" hidden>
    <blockquote>Received response<span
                id="execution-response-status-DELETEapi-logout"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-DELETEapi-logout"></code></pre>
</span>
<span id="execution-error-DELETEapi-logout" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-DELETEapi-logout"></code></pre>
</span>
<form id="form-DELETEapi-logout" data-method="DELETE"
      data-path="api/logout"
      data-authed="0"
      data-hasfiles="0"
      data-isarraybody="0"
      data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}'
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('DELETEapi-logout', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-DELETEapi-logout"
                    onclick="tryItOut('DELETEapi-logout');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-DELETEapi-logout"
                    onclick="cancelTryOut('DELETEapi-logout');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-DELETEapi-logout" hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-red">DELETE</small>
            <b><code>api/logout</code></b>
        </p>
                    </form>

            <h2 id="endpoints-GETapi-email-verify--id---hash-">GET api/email/verify/{id}/{hash}</h2>

<p>
</p>



<span id="example-requests-GETapi-email-verify--id---hash-">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request GET \
    --get "availability-management.test/api/email/verify/non/sunt" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json"</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "availability-management.test/api/email/verify/non/sunt"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers,
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-GETapi-email-verify--id---hash-">
            <blockquote>
            <p>Example response (401):</p>
        </blockquote>
                <details class="annotation">
            <summary>
                <small onclick="textContent = parentElement.parentElement.open ? 'Show headers' : 'Hide headers'">Show headers</small>
            </summary>
            <pre><code class="language-http">cache-control: no-cache, private
content-type: application/json
access-control-allow-origin: *
 </code></pre>
        </details>         <pre>

<code class="language-json">{
    &quot;message&quot;: &quot;Unauthenticated.&quot;
}</code>
 </pre>
    </span>
<span id="execution-results-GETapi-email-verify--id---hash-" hidden>
    <blockquote>Received response<span
                id="execution-response-status-GETapi-email-verify--id---hash-"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-email-verify--id---hash-"></code></pre>
</span>
<span id="execution-error-GETapi-email-verify--id---hash-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-email-verify--id---hash-"></code></pre>
</span>
<form id="form-GETapi-email-verify--id---hash-" data-method="GET"
      data-path="api/email/verify/{id}/{hash}"
      data-authed="0"
      data-hasfiles="0"
      data-isarraybody="0"
      data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}'
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('GETapi-email-verify--id---hash-', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-GETapi-email-verify--id---hash-"
                    onclick="tryItOut('GETapi-email-verify--id---hash-');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-GETapi-email-verify--id---hash-"
                    onclick="cancelTryOut('GETapi-email-verify--id---hash-');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-GETapi-email-verify--id---hash-" hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-green">GET</small>
            <b><code>api/email/verify/{id}/{hash}</code></b>
        </p>
                    <h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
                    <p>
                <b><code>id</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
                <input type="text"
               name="id"
               data-endpoint="GETapi-email-verify--id---hash-"
               value="non"
               data-component="url" hidden>
    <br>
<p>The ID of the verify.</p>
            </p>
                    <p>
                <b><code>hash</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
                <input type="text"
               name="hash"
               data-endpoint="GETapi-email-verify--id---hash-"
               value="sunt"
               data-component="url" hidden>
    <br>

            </p>
                    </form>

            <h2 id="endpoints-POSTapi-email-verification-notification">POST api/email/verification-notification</h2>

<p>
</p>



<span id="example-requests-POSTapi-email-verification-notification">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request POST \
    "availability-management.test/api/email/verification-notification" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json"</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "availability-management.test/api/email/verification-notification"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers,
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-POSTapi-email-verification-notification">
</span>
<span id="execution-results-POSTapi-email-verification-notification" hidden>
    <blockquote>Received response<span
                id="execution-response-status-POSTapi-email-verification-notification"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-email-verification-notification"></code></pre>
</span>
<span id="execution-error-POSTapi-email-verification-notification" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-email-verification-notification"></code></pre>
</span>
<form id="form-POSTapi-email-verification-notification" data-method="POST"
      data-path="api/email/verification-notification"
      data-authed="0"
      data-hasfiles="0"
      data-isarraybody="0"
      data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}'
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('POSTapi-email-verification-notification', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-POSTapi-email-verification-notification"
                    onclick="tryItOut('POSTapi-email-verification-notification');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-POSTapi-email-verification-notification"
                    onclick="cancelTryOut('POSTapi-email-verification-notification');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-POSTapi-email-verification-notification" hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-black">POST</small>
            <b><code>api/email/verification-notification</code></b>
        </p>
                    </form>

            <h2 id="endpoints-GETapi-availabilities">Return all availabilities.</h2>

<p>
<small class="badge badge-darkred">requires authentication</small>
</p>



<span id="example-requests-GETapi-availabilities">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request GET \
    --get "availability-management.test/api/availabilities" \
    --header "Authorization: Bearer {YOUR_AUTH_KEY}" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json"</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "availability-management.test/api/availabilities"
);

const headers = {
    "Authorization": "Bearer {YOUR_AUTH_KEY}",
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers,
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-GETapi-availabilities">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <details class="annotation">
            <summary>
                <small onclick="textContent = parentElement.parentElement.open ? 'Show headers' : 'Hide headers'">Show headers</small>
            </summary>
            <pre><code class="language-http">cache-control: no-cache, private
content-type: application/json
x-ratelimit-limit: 60
x-ratelimit-remaining: 57
access-control-allow-origin: *
 </code></pre>
        </details>         <pre>

<code class="language-json">{
    &quot;availabilities&quot;: [
        {
            &quot;start&quot;: &quot;2022-04-01T17:04:42.000000Z&quot;,
            &quot;end&quot;: &quot;2022-04-01T17:05:41.000000Z&quot;,
            &quot;is_recurring&quot;: 0,
            &quot;user&quot;: {
                &quot;id&quot;: 3,
                &quot;name&quot;: &quot;Pera&quot;,
                &quot;email&quot;: &quot;pera@yopmail.com&quot;,
                &quot;timezone&quot;: &quot;Europe/Belgrade&quot;,
                &quot;availabilities&quot;: [
                    {
                        &quot;start&quot;: &quot;2022-04-01T17:04:42.000000Z&quot;,
                        &quot;end&quot;: &quot;2022-04-01T17:05:41.000000Z&quot;,
                        &quot;is_recurring&quot;: 0,
                        &quot;user_id&quot;: 3,
                        &quot;created_at&quot;: &quot;2022-04-05T20:56:41.000000Z&quot;,
                        &quot;updated_at&quot;: &quot;2022-04-05T20:56:41.000000Z&quot;
                    },
                    {
                        &quot;start&quot;: &quot;2022-04-01T17:01:42.000000Z&quot;,
                        &quot;end&quot;: &quot;2022-04-01T17:02:41.000000Z&quot;,
                        &quot;is_recurring&quot;: 0,
                        &quot;user_id&quot;: 3,
                        &quot;created_at&quot;: &quot;2022-04-05T20:56:41.000000Z&quot;,
                        &quot;updated_at&quot;: &quot;2022-04-05T20:56:41.000000Z&quot;
                    },
                    {
                        &quot;start&quot;: &quot;2022-04-01T17:04:42.000000Z&quot;,
                        &quot;end&quot;: &quot;2022-04-01T17:05:41.000000Z&quot;,
                        &quot;is_recurring&quot;: 0,
                        &quot;user_id&quot;: 3,
                        &quot;created_at&quot;: &quot;2022-04-05T20:56:41.000000Z&quot;,
                        &quot;updated_at&quot;: &quot;2022-04-05T20:56:41.000000Z&quot;
                    },
                    {
                        &quot;start&quot;: &quot;2022-04-01T17:04:42.000000Z&quot;,
                        &quot;end&quot;: &quot;2022-04-01T17:05:41.000000Z&quot;,
                        &quot;is_recurring&quot;: 0,
                        &quot;user_id&quot;: 3,
                        &quot;created_at&quot;: &quot;2022-04-05T20:56:41.000000Z&quot;,
                        &quot;updated_at&quot;: &quot;2022-04-05T20:56:41.000000Z&quot;
                    },
                    {
                        &quot;start&quot;: &quot;2022-04-01T16:04:43.000000Z&quot;,
                        &quot;end&quot;: &quot;2022-04-01T16:04:42.000000Z&quot;,
                        &quot;is_recurring&quot;: 1,
                        &quot;user_id&quot;: 3,
                        &quot;created_at&quot;: &quot;2022-04-06T17:56:17.000000Z&quot;,
                        &quot;updated_at&quot;: &quot;2022-04-06T17:56:17.000000Z&quot;
                    },
                    {
                        &quot;start&quot;: &quot;2022-04-08T16:04:43.000000Z&quot;,
                        &quot;end&quot;: &quot;2022-04-08T16:04:42.000000Z&quot;,
                        &quot;is_recurring&quot;: 1,
                        &quot;user_id&quot;: 3,
                        &quot;created_at&quot;: &quot;2022-04-06T17:56:17.000000Z&quot;,
                        &quot;updated_at&quot;: &quot;2022-04-06T17:56:17.000000Z&quot;
                    },
                    {
                        &quot;start&quot;: &quot;2022-04-15T16:04:43.000000Z&quot;,
                        &quot;end&quot;: &quot;2022-04-15T16:04:42.000000Z&quot;,
                        &quot;is_recurring&quot;: 1,
                        &quot;user_id&quot;: 3,
                        &quot;created_at&quot;: &quot;2022-04-06T17:56:17.000000Z&quot;,
                        &quot;updated_at&quot;: &quot;2022-04-06T17:56:17.000000Z&quot;
                    },
                    {
                        &quot;start&quot;: &quot;2022-04-22T16:04:43.000000Z&quot;,
                        &quot;end&quot;: &quot;2022-04-22T16:04:42.000000Z&quot;,
                        &quot;is_recurring&quot;: 1,
                        &quot;user_id&quot;: 3,
                        &quot;created_at&quot;: &quot;2022-04-06T17:56:17.000000Z&quot;,
                        &quot;updated_at&quot;: &quot;2022-04-06T17:56:17.000000Z&quot;
                    },
                    {
                        &quot;start&quot;: &quot;2022-04-29T16:04:43.000000Z&quot;,
                        &quot;end&quot;: &quot;2022-04-29T16:04:42.000000Z&quot;,
                        &quot;is_recurring&quot;: 1,
                        &quot;user_id&quot;: 3,
                        &quot;created_at&quot;: &quot;2022-04-06T17:56:17.000000Z&quot;,
                        &quot;updated_at&quot;: &quot;2022-04-06T17:56:17.000000Z&quot;
                    }
                ],
                &quot;created_at&quot;: &quot;2022-04-01T23:06:22.000000Z&quot;,
                &quot;updated_at&quot;: &quot;2022-04-01T23:06:22.000000Z&quot;
            }
        },
        {
            &quot;start&quot;: &quot;2022-04-01T17:01:42.000000Z&quot;,
            &quot;end&quot;: &quot;2022-04-01T17:02:41.000000Z&quot;,
            &quot;is_recurring&quot;: 0,
            &quot;user&quot;: {
                &quot;id&quot;: 3,
                &quot;name&quot;: &quot;Pera&quot;,
                &quot;email&quot;: &quot;pera@yopmail.com&quot;,
                &quot;timezone&quot;: &quot;Europe/Belgrade&quot;,
                &quot;availabilities&quot;: [
                    {
                        &quot;start&quot;: &quot;2022-04-01T17:04:42.000000Z&quot;,
                        &quot;end&quot;: &quot;2022-04-01T17:05:41.000000Z&quot;,
                        &quot;is_recurring&quot;: 0,
                        &quot;user_id&quot;: 3,
                        &quot;created_at&quot;: &quot;2022-04-05T20:56:41.000000Z&quot;,
                        &quot;updated_at&quot;: &quot;2022-04-05T20:56:41.000000Z&quot;
                    },
                    {
                        &quot;start&quot;: &quot;2022-04-01T17:01:42.000000Z&quot;,
                        &quot;end&quot;: &quot;2022-04-01T17:02:41.000000Z&quot;,
                        &quot;is_recurring&quot;: 0,
                        &quot;user_id&quot;: 3,
                        &quot;created_at&quot;: &quot;2022-04-05T20:56:41.000000Z&quot;,
                        &quot;updated_at&quot;: &quot;2022-04-05T20:56:41.000000Z&quot;
                    },
                    {
                        &quot;start&quot;: &quot;2022-04-01T17:04:42.000000Z&quot;,
                        &quot;end&quot;: &quot;2022-04-01T17:05:41.000000Z&quot;,
                        &quot;is_recurring&quot;: 0,
                        &quot;user_id&quot;: 3,
                        &quot;created_at&quot;: &quot;2022-04-05T20:56:41.000000Z&quot;,
                        &quot;updated_at&quot;: &quot;2022-04-05T20:56:41.000000Z&quot;
                    },
                    {
                        &quot;start&quot;: &quot;2022-04-01T17:04:42.000000Z&quot;,
                        &quot;end&quot;: &quot;2022-04-01T17:05:41.000000Z&quot;,
                        &quot;is_recurring&quot;: 0,
                        &quot;user_id&quot;: 3,
                        &quot;created_at&quot;: &quot;2022-04-05T20:56:41.000000Z&quot;,
                        &quot;updated_at&quot;: &quot;2022-04-05T20:56:41.000000Z&quot;
                    },
                    {
                        &quot;start&quot;: &quot;2022-04-01T16:04:43.000000Z&quot;,
                        &quot;end&quot;: &quot;2022-04-01T16:04:42.000000Z&quot;,
                        &quot;is_recurring&quot;: 1,
                        &quot;user_id&quot;: 3,
                        &quot;created_at&quot;: &quot;2022-04-06T17:56:17.000000Z&quot;,
                        &quot;updated_at&quot;: &quot;2022-04-06T17:56:17.000000Z&quot;
                    },
                    {
                        &quot;start&quot;: &quot;2022-04-08T16:04:43.000000Z&quot;,
                        &quot;end&quot;: &quot;2022-04-08T16:04:42.000000Z&quot;,
                        &quot;is_recurring&quot;: 1,
                        &quot;user_id&quot;: 3,
                        &quot;created_at&quot;: &quot;2022-04-06T17:56:17.000000Z&quot;,
                        &quot;updated_at&quot;: &quot;2022-04-06T17:56:17.000000Z&quot;
                    },
                    {
                        &quot;start&quot;: &quot;2022-04-15T16:04:43.000000Z&quot;,
                        &quot;end&quot;: &quot;2022-04-15T16:04:42.000000Z&quot;,
                        &quot;is_recurring&quot;: 1,
                        &quot;user_id&quot;: 3,
                        &quot;created_at&quot;: &quot;2022-04-06T17:56:17.000000Z&quot;,
                        &quot;updated_at&quot;: &quot;2022-04-06T17:56:17.000000Z&quot;
                    },
                    {
                        &quot;start&quot;: &quot;2022-04-22T16:04:43.000000Z&quot;,
                        &quot;end&quot;: &quot;2022-04-22T16:04:42.000000Z&quot;,
                        &quot;is_recurring&quot;: 1,
                        &quot;user_id&quot;: 3,
                        &quot;created_at&quot;: &quot;2022-04-06T17:56:17.000000Z&quot;,
                        &quot;updated_at&quot;: &quot;2022-04-06T17:56:17.000000Z&quot;
                    },
                    {
                        &quot;start&quot;: &quot;2022-04-29T16:04:43.000000Z&quot;,
                        &quot;end&quot;: &quot;2022-04-29T16:04:42.000000Z&quot;,
                        &quot;is_recurring&quot;: 1,
                        &quot;user_id&quot;: 3,
                        &quot;created_at&quot;: &quot;2022-04-06T17:56:17.000000Z&quot;,
                        &quot;updated_at&quot;: &quot;2022-04-06T17:56:17.000000Z&quot;
                    }
                ],
                &quot;created_at&quot;: &quot;2022-04-01T23:06:22.000000Z&quot;,
                &quot;updated_at&quot;: &quot;2022-04-01T23:06:22.000000Z&quot;
            }
        },
        {
            &quot;start&quot;: &quot;2022-04-01T17:04:42.000000Z&quot;,
            &quot;end&quot;: &quot;2022-04-01T17:05:41.000000Z&quot;,
            &quot;is_recurring&quot;: 0,
            &quot;user&quot;: {
                &quot;id&quot;: 3,
                &quot;name&quot;: &quot;Pera&quot;,
                &quot;email&quot;: &quot;pera@yopmail.com&quot;,
                &quot;timezone&quot;: &quot;Europe/Belgrade&quot;,
                &quot;availabilities&quot;: [
                    {
                        &quot;start&quot;: &quot;2022-04-01T17:04:42.000000Z&quot;,
                        &quot;end&quot;: &quot;2022-04-01T17:05:41.000000Z&quot;,
                        &quot;is_recurring&quot;: 0,
                        &quot;user_id&quot;: 3,
                        &quot;created_at&quot;: &quot;2022-04-05T20:56:41.000000Z&quot;,
                        &quot;updated_at&quot;: &quot;2022-04-05T20:56:41.000000Z&quot;
                    },
                    {
                        &quot;start&quot;: &quot;2022-04-01T17:01:42.000000Z&quot;,
                        &quot;end&quot;: &quot;2022-04-01T17:02:41.000000Z&quot;,
                        &quot;is_recurring&quot;: 0,
                        &quot;user_id&quot;: 3,
                        &quot;created_at&quot;: &quot;2022-04-05T20:56:41.000000Z&quot;,
                        &quot;updated_at&quot;: &quot;2022-04-05T20:56:41.000000Z&quot;
                    },
                    {
                        &quot;start&quot;: &quot;2022-04-01T17:04:42.000000Z&quot;,
                        &quot;end&quot;: &quot;2022-04-01T17:05:41.000000Z&quot;,
                        &quot;is_recurring&quot;: 0,
                        &quot;user_id&quot;: 3,
                        &quot;created_at&quot;: &quot;2022-04-05T20:56:41.000000Z&quot;,
                        &quot;updated_at&quot;: &quot;2022-04-05T20:56:41.000000Z&quot;
                    },
                    {
                        &quot;start&quot;: &quot;2022-04-01T17:04:42.000000Z&quot;,
                        &quot;end&quot;: &quot;2022-04-01T17:05:41.000000Z&quot;,
                        &quot;is_recurring&quot;: 0,
                        &quot;user_id&quot;: 3,
                        &quot;created_at&quot;: &quot;2022-04-05T20:56:41.000000Z&quot;,
                        &quot;updated_at&quot;: &quot;2022-04-05T20:56:41.000000Z&quot;
                    },
                    {
                        &quot;start&quot;: &quot;2022-04-01T16:04:43.000000Z&quot;,
                        &quot;end&quot;: &quot;2022-04-01T16:04:42.000000Z&quot;,
                        &quot;is_recurring&quot;: 1,
                        &quot;user_id&quot;: 3,
                        &quot;created_at&quot;: &quot;2022-04-06T17:56:17.000000Z&quot;,
                        &quot;updated_at&quot;: &quot;2022-04-06T17:56:17.000000Z&quot;
                    },
                    {
                        &quot;start&quot;: &quot;2022-04-08T16:04:43.000000Z&quot;,
                        &quot;end&quot;: &quot;2022-04-08T16:04:42.000000Z&quot;,
                        &quot;is_recurring&quot;: 1,
                        &quot;user_id&quot;: 3,
                        &quot;created_at&quot;: &quot;2022-04-06T17:56:17.000000Z&quot;,
                        &quot;updated_at&quot;: &quot;2022-04-06T17:56:17.000000Z&quot;
                    },
                    {
                        &quot;start&quot;: &quot;2022-04-15T16:04:43.000000Z&quot;,
                        &quot;end&quot;: &quot;2022-04-15T16:04:42.000000Z&quot;,
                        &quot;is_recurring&quot;: 1,
                        &quot;user_id&quot;: 3,
                        &quot;created_at&quot;: &quot;2022-04-06T17:56:17.000000Z&quot;,
                        &quot;updated_at&quot;: &quot;2022-04-06T17:56:17.000000Z&quot;
                    },
                    {
                        &quot;start&quot;: &quot;2022-04-22T16:04:43.000000Z&quot;,
                        &quot;end&quot;: &quot;2022-04-22T16:04:42.000000Z&quot;,
                        &quot;is_recurring&quot;: 1,
                        &quot;user_id&quot;: 3,
                        &quot;created_at&quot;: &quot;2022-04-06T17:56:17.000000Z&quot;,
                        &quot;updated_at&quot;: &quot;2022-04-06T17:56:17.000000Z&quot;
                    },
                    {
                        &quot;start&quot;: &quot;2022-04-29T16:04:43.000000Z&quot;,
                        &quot;end&quot;: &quot;2022-04-29T16:04:42.000000Z&quot;,
                        &quot;is_recurring&quot;: 1,
                        &quot;user_id&quot;: 3,
                        &quot;created_at&quot;: &quot;2022-04-06T17:56:17.000000Z&quot;,
                        &quot;updated_at&quot;: &quot;2022-04-06T17:56:17.000000Z&quot;
                    }
                ],
                &quot;created_at&quot;: &quot;2022-04-01T23:06:22.000000Z&quot;,
                &quot;updated_at&quot;: &quot;2022-04-01T23:06:22.000000Z&quot;
            }
        },
        {
            &quot;start&quot;: &quot;2022-04-01T17:04:42.000000Z&quot;,
            &quot;end&quot;: &quot;2022-04-01T17:05:41.000000Z&quot;,
            &quot;is_recurring&quot;: 0,
            &quot;user&quot;: {
                &quot;id&quot;: 3,
                &quot;name&quot;: &quot;Pera&quot;,
                &quot;email&quot;: &quot;pera@yopmail.com&quot;,
                &quot;timezone&quot;: &quot;Europe/Belgrade&quot;,
                &quot;availabilities&quot;: [
                    {
                        &quot;start&quot;: &quot;2022-04-01T17:04:42.000000Z&quot;,
                        &quot;end&quot;: &quot;2022-04-01T17:05:41.000000Z&quot;,
                        &quot;is_recurring&quot;: 0,
                        &quot;user_id&quot;: 3,
                        &quot;created_at&quot;: &quot;2022-04-05T20:56:41.000000Z&quot;,
                        &quot;updated_at&quot;: &quot;2022-04-05T20:56:41.000000Z&quot;
                    },
                    {
                        &quot;start&quot;: &quot;2022-04-01T17:01:42.000000Z&quot;,
                        &quot;end&quot;: &quot;2022-04-01T17:02:41.000000Z&quot;,
                        &quot;is_recurring&quot;: 0,
                        &quot;user_id&quot;: 3,
                        &quot;created_at&quot;: &quot;2022-04-05T20:56:41.000000Z&quot;,
                        &quot;updated_at&quot;: &quot;2022-04-05T20:56:41.000000Z&quot;
                    },
                    {
                        &quot;start&quot;: &quot;2022-04-01T17:04:42.000000Z&quot;,
                        &quot;end&quot;: &quot;2022-04-01T17:05:41.000000Z&quot;,
                        &quot;is_recurring&quot;: 0,
                        &quot;user_id&quot;: 3,
                        &quot;created_at&quot;: &quot;2022-04-05T20:56:41.000000Z&quot;,
                        &quot;updated_at&quot;: &quot;2022-04-05T20:56:41.000000Z&quot;
                    },
                    {
                        &quot;start&quot;: &quot;2022-04-01T17:04:42.000000Z&quot;,
                        &quot;end&quot;: &quot;2022-04-01T17:05:41.000000Z&quot;,
                        &quot;is_recurring&quot;: 0,
                        &quot;user_id&quot;: 3,
                        &quot;created_at&quot;: &quot;2022-04-05T20:56:41.000000Z&quot;,
                        &quot;updated_at&quot;: &quot;2022-04-05T20:56:41.000000Z&quot;
                    },
                    {
                        &quot;start&quot;: &quot;2022-04-01T16:04:43.000000Z&quot;,
                        &quot;end&quot;: &quot;2022-04-01T16:04:42.000000Z&quot;,
                        &quot;is_recurring&quot;: 1,
                        &quot;user_id&quot;: 3,
                        &quot;created_at&quot;: &quot;2022-04-06T17:56:17.000000Z&quot;,
                        &quot;updated_at&quot;: &quot;2022-04-06T17:56:17.000000Z&quot;
                    },
                    {
                        &quot;start&quot;: &quot;2022-04-08T16:04:43.000000Z&quot;,
                        &quot;end&quot;: &quot;2022-04-08T16:04:42.000000Z&quot;,
                        &quot;is_recurring&quot;: 1,
                        &quot;user_id&quot;: 3,
                        &quot;created_at&quot;: &quot;2022-04-06T17:56:17.000000Z&quot;,
                        &quot;updated_at&quot;: &quot;2022-04-06T17:56:17.000000Z&quot;
                    },
                    {
                        &quot;start&quot;: &quot;2022-04-15T16:04:43.000000Z&quot;,
                        &quot;end&quot;: &quot;2022-04-15T16:04:42.000000Z&quot;,
                        &quot;is_recurring&quot;: 1,
                        &quot;user_id&quot;: 3,
                        &quot;created_at&quot;: &quot;2022-04-06T17:56:17.000000Z&quot;,
                        &quot;updated_at&quot;: &quot;2022-04-06T17:56:17.000000Z&quot;
                    },
                    {
                        &quot;start&quot;: &quot;2022-04-22T16:04:43.000000Z&quot;,
                        &quot;end&quot;: &quot;2022-04-22T16:04:42.000000Z&quot;,
                        &quot;is_recurring&quot;: 1,
                        &quot;user_id&quot;: 3,
                        &quot;created_at&quot;: &quot;2022-04-06T17:56:17.000000Z&quot;,
                        &quot;updated_at&quot;: &quot;2022-04-06T17:56:17.000000Z&quot;
                    },
                    {
                        &quot;start&quot;: &quot;2022-04-29T16:04:43.000000Z&quot;,
                        &quot;end&quot;: &quot;2022-04-29T16:04:42.000000Z&quot;,
                        &quot;is_recurring&quot;: 1,
                        &quot;user_id&quot;: 3,
                        &quot;created_at&quot;: &quot;2022-04-06T17:56:17.000000Z&quot;,
                        &quot;updated_at&quot;: &quot;2022-04-06T17:56:17.000000Z&quot;
                    }
                ],
                &quot;created_at&quot;: &quot;2022-04-01T23:06:22.000000Z&quot;,
                &quot;updated_at&quot;: &quot;2022-04-01T23:06:22.000000Z&quot;
            }
        },
        {
            &quot;start&quot;: &quot;2022-04-01T16:04:43.000000Z&quot;,
            &quot;end&quot;: &quot;2022-04-01T16:04:42.000000Z&quot;,
            &quot;is_recurring&quot;: 1,
            &quot;user&quot;: {
                &quot;id&quot;: 3,
                &quot;name&quot;: &quot;Pera&quot;,
                &quot;email&quot;: &quot;pera@yopmail.com&quot;,
                &quot;timezone&quot;: &quot;Europe/Belgrade&quot;,
                &quot;availabilities&quot;: [
                    {
                        &quot;start&quot;: &quot;2022-04-01T17:04:42.000000Z&quot;,
                        &quot;end&quot;: &quot;2022-04-01T17:05:41.000000Z&quot;,
                        &quot;is_recurring&quot;: 0,
                        &quot;user_id&quot;: 3,
                        &quot;created_at&quot;: &quot;2022-04-05T20:56:41.000000Z&quot;,
                        &quot;updated_at&quot;: &quot;2022-04-05T20:56:41.000000Z&quot;
                    },
                    {
                        &quot;start&quot;: &quot;2022-04-01T17:01:42.000000Z&quot;,
                        &quot;end&quot;: &quot;2022-04-01T17:02:41.000000Z&quot;,
                        &quot;is_recurring&quot;: 0,
                        &quot;user_id&quot;: 3,
                        &quot;created_at&quot;: &quot;2022-04-05T20:56:41.000000Z&quot;,
                        &quot;updated_at&quot;: &quot;2022-04-05T20:56:41.000000Z&quot;
                    },
                    {
                        &quot;start&quot;: &quot;2022-04-01T17:04:42.000000Z&quot;,
                        &quot;end&quot;: &quot;2022-04-01T17:05:41.000000Z&quot;,
                        &quot;is_recurring&quot;: 0,
                        &quot;user_id&quot;: 3,
                        &quot;created_at&quot;: &quot;2022-04-05T20:56:41.000000Z&quot;,
                        &quot;updated_at&quot;: &quot;2022-04-05T20:56:41.000000Z&quot;
                    },
                    {
                        &quot;start&quot;: &quot;2022-04-01T17:04:42.000000Z&quot;,
                        &quot;end&quot;: &quot;2022-04-01T17:05:41.000000Z&quot;,
                        &quot;is_recurring&quot;: 0,
                        &quot;user_id&quot;: 3,
                        &quot;created_at&quot;: &quot;2022-04-05T20:56:41.000000Z&quot;,
                        &quot;updated_at&quot;: &quot;2022-04-05T20:56:41.000000Z&quot;
                    },
                    {
                        &quot;start&quot;: &quot;2022-04-01T16:04:43.000000Z&quot;,
                        &quot;end&quot;: &quot;2022-04-01T16:04:42.000000Z&quot;,
                        &quot;is_recurring&quot;: 1,
                        &quot;user_id&quot;: 3,
                        &quot;created_at&quot;: &quot;2022-04-06T17:56:17.000000Z&quot;,
                        &quot;updated_at&quot;: &quot;2022-04-06T17:56:17.000000Z&quot;
                    },
                    {
                        &quot;start&quot;: &quot;2022-04-08T16:04:43.000000Z&quot;,
                        &quot;end&quot;: &quot;2022-04-08T16:04:42.000000Z&quot;,
                        &quot;is_recurring&quot;: 1,
                        &quot;user_id&quot;: 3,
                        &quot;created_at&quot;: &quot;2022-04-06T17:56:17.000000Z&quot;,
                        &quot;updated_at&quot;: &quot;2022-04-06T17:56:17.000000Z&quot;
                    },
                    {
                        &quot;start&quot;: &quot;2022-04-15T16:04:43.000000Z&quot;,
                        &quot;end&quot;: &quot;2022-04-15T16:04:42.000000Z&quot;,
                        &quot;is_recurring&quot;: 1,
                        &quot;user_id&quot;: 3,
                        &quot;created_at&quot;: &quot;2022-04-06T17:56:17.000000Z&quot;,
                        &quot;updated_at&quot;: &quot;2022-04-06T17:56:17.000000Z&quot;
                    },
                    {
                        &quot;start&quot;: &quot;2022-04-22T16:04:43.000000Z&quot;,
                        &quot;end&quot;: &quot;2022-04-22T16:04:42.000000Z&quot;,
                        &quot;is_recurring&quot;: 1,
                        &quot;user_id&quot;: 3,
                        &quot;created_at&quot;: &quot;2022-04-06T17:56:17.000000Z&quot;,
                        &quot;updated_at&quot;: &quot;2022-04-06T17:56:17.000000Z&quot;
                    },
                    {
                        &quot;start&quot;: &quot;2022-04-29T16:04:43.000000Z&quot;,
                        &quot;end&quot;: &quot;2022-04-29T16:04:42.000000Z&quot;,
                        &quot;is_recurring&quot;: 1,
                        &quot;user_id&quot;: 3,
                        &quot;created_at&quot;: &quot;2022-04-06T17:56:17.000000Z&quot;,
                        &quot;updated_at&quot;: &quot;2022-04-06T17:56:17.000000Z&quot;
                    }
                ],
                &quot;created_at&quot;: &quot;2022-04-01T23:06:22.000000Z&quot;,
                &quot;updated_at&quot;: &quot;2022-04-01T23:06:22.000000Z&quot;
            }
        },
        {
            &quot;start&quot;: &quot;2022-04-08T16:04:43.000000Z&quot;,
            &quot;end&quot;: &quot;2022-04-08T16:04:42.000000Z&quot;,
            &quot;is_recurring&quot;: 1,
            &quot;user&quot;: {
                &quot;id&quot;: 3,
                &quot;name&quot;: &quot;Pera&quot;,
                &quot;email&quot;: &quot;pera@yopmail.com&quot;,
                &quot;timezone&quot;: &quot;Europe/Belgrade&quot;,
                &quot;availabilities&quot;: [
                    {
                        &quot;start&quot;: &quot;2022-04-01T17:04:42.000000Z&quot;,
                        &quot;end&quot;: &quot;2022-04-01T17:05:41.000000Z&quot;,
                        &quot;is_recurring&quot;: 0,
                        &quot;user_id&quot;: 3,
                        &quot;created_at&quot;: &quot;2022-04-05T20:56:41.000000Z&quot;,
                        &quot;updated_at&quot;: &quot;2022-04-05T20:56:41.000000Z&quot;
                    },
                    {
                        &quot;start&quot;: &quot;2022-04-01T17:01:42.000000Z&quot;,
                        &quot;end&quot;: &quot;2022-04-01T17:02:41.000000Z&quot;,
                        &quot;is_recurring&quot;: 0,
                        &quot;user_id&quot;: 3,
                        &quot;created_at&quot;: &quot;2022-04-05T20:56:41.000000Z&quot;,
                        &quot;updated_at&quot;: &quot;2022-04-05T20:56:41.000000Z&quot;
                    },
                    {
                        &quot;start&quot;: &quot;2022-04-01T17:04:42.000000Z&quot;,
                        &quot;end&quot;: &quot;2022-04-01T17:05:41.000000Z&quot;,
                        &quot;is_recurring&quot;: 0,
                        &quot;user_id&quot;: 3,
                        &quot;created_at&quot;: &quot;2022-04-05T20:56:41.000000Z&quot;,
                        &quot;updated_at&quot;: &quot;2022-04-05T20:56:41.000000Z&quot;
                    },
                    {
                        &quot;start&quot;: &quot;2022-04-01T17:04:42.000000Z&quot;,
                        &quot;end&quot;: &quot;2022-04-01T17:05:41.000000Z&quot;,
                        &quot;is_recurring&quot;: 0,
                        &quot;user_id&quot;: 3,
                        &quot;created_at&quot;: &quot;2022-04-05T20:56:41.000000Z&quot;,
                        &quot;updated_at&quot;: &quot;2022-04-05T20:56:41.000000Z&quot;
                    },
                    {
                        &quot;start&quot;: &quot;2022-04-01T16:04:43.000000Z&quot;,
                        &quot;end&quot;: &quot;2022-04-01T16:04:42.000000Z&quot;,
                        &quot;is_recurring&quot;: 1,
                        &quot;user_id&quot;: 3,
                        &quot;created_at&quot;: &quot;2022-04-06T17:56:17.000000Z&quot;,
                        &quot;updated_at&quot;: &quot;2022-04-06T17:56:17.000000Z&quot;
                    },
                    {
                        &quot;start&quot;: &quot;2022-04-08T16:04:43.000000Z&quot;,
                        &quot;end&quot;: &quot;2022-04-08T16:04:42.000000Z&quot;,
                        &quot;is_recurring&quot;: 1,
                        &quot;user_id&quot;: 3,
                        &quot;created_at&quot;: &quot;2022-04-06T17:56:17.000000Z&quot;,
                        &quot;updated_at&quot;: &quot;2022-04-06T17:56:17.000000Z&quot;
                    },
                    {
                        &quot;start&quot;: &quot;2022-04-15T16:04:43.000000Z&quot;,
                        &quot;end&quot;: &quot;2022-04-15T16:04:42.000000Z&quot;,
                        &quot;is_recurring&quot;: 1,
                        &quot;user_id&quot;: 3,
                        &quot;created_at&quot;: &quot;2022-04-06T17:56:17.000000Z&quot;,
                        &quot;updated_at&quot;: &quot;2022-04-06T17:56:17.000000Z&quot;
                    },
                    {
                        &quot;start&quot;: &quot;2022-04-22T16:04:43.000000Z&quot;,
                        &quot;end&quot;: &quot;2022-04-22T16:04:42.000000Z&quot;,
                        &quot;is_recurring&quot;: 1,
                        &quot;user_id&quot;: 3,
                        &quot;created_at&quot;: &quot;2022-04-06T17:56:17.000000Z&quot;,
                        &quot;updated_at&quot;: &quot;2022-04-06T17:56:17.000000Z&quot;
                    },
                    {
                        &quot;start&quot;: &quot;2022-04-29T16:04:43.000000Z&quot;,
                        &quot;end&quot;: &quot;2022-04-29T16:04:42.000000Z&quot;,
                        &quot;is_recurring&quot;: 1,
                        &quot;user_id&quot;: 3,
                        &quot;created_at&quot;: &quot;2022-04-06T17:56:17.000000Z&quot;,
                        &quot;updated_at&quot;: &quot;2022-04-06T17:56:17.000000Z&quot;
                    }
                ],
                &quot;created_at&quot;: &quot;2022-04-01T23:06:22.000000Z&quot;,
                &quot;updated_at&quot;: &quot;2022-04-01T23:06:22.000000Z&quot;
            }
        },
        {
            &quot;start&quot;: &quot;2022-04-15T16:04:43.000000Z&quot;,
            &quot;end&quot;: &quot;2022-04-15T16:04:42.000000Z&quot;,
            &quot;is_recurring&quot;: 1,
            &quot;user&quot;: {
                &quot;id&quot;: 3,
                &quot;name&quot;: &quot;Pera&quot;,
                &quot;email&quot;: &quot;pera@yopmail.com&quot;,
                &quot;timezone&quot;: &quot;Europe/Belgrade&quot;,
                &quot;availabilities&quot;: [
                    {
                        &quot;start&quot;: &quot;2022-04-01T17:04:42.000000Z&quot;,
                        &quot;end&quot;: &quot;2022-04-01T17:05:41.000000Z&quot;,
                        &quot;is_recurring&quot;: 0,
                        &quot;user_id&quot;: 3,
                        &quot;created_at&quot;: &quot;2022-04-05T20:56:41.000000Z&quot;,
                        &quot;updated_at&quot;: &quot;2022-04-05T20:56:41.000000Z&quot;
                    },
                    {
                        &quot;start&quot;: &quot;2022-04-01T17:01:42.000000Z&quot;,
                        &quot;end&quot;: &quot;2022-04-01T17:02:41.000000Z&quot;,
                        &quot;is_recurring&quot;: 0,
                        &quot;user_id&quot;: 3,
                        &quot;created_at&quot;: &quot;2022-04-05T20:56:41.000000Z&quot;,
                        &quot;updated_at&quot;: &quot;2022-04-05T20:56:41.000000Z&quot;
                    },
                    {
                        &quot;start&quot;: &quot;2022-04-01T17:04:42.000000Z&quot;,
                        &quot;end&quot;: &quot;2022-04-01T17:05:41.000000Z&quot;,
                        &quot;is_recurring&quot;: 0,
                        &quot;user_id&quot;: 3,
                        &quot;created_at&quot;: &quot;2022-04-05T20:56:41.000000Z&quot;,
                        &quot;updated_at&quot;: &quot;2022-04-05T20:56:41.000000Z&quot;
                    },
                    {
                        &quot;start&quot;: &quot;2022-04-01T17:04:42.000000Z&quot;,
                        &quot;end&quot;: &quot;2022-04-01T17:05:41.000000Z&quot;,
                        &quot;is_recurring&quot;: 0,
                        &quot;user_id&quot;: 3,
                        &quot;created_at&quot;: &quot;2022-04-05T20:56:41.000000Z&quot;,
                        &quot;updated_at&quot;: &quot;2022-04-05T20:56:41.000000Z&quot;
                    },
                    {
                        &quot;start&quot;: &quot;2022-04-01T16:04:43.000000Z&quot;,
                        &quot;end&quot;: &quot;2022-04-01T16:04:42.000000Z&quot;,
                        &quot;is_recurring&quot;: 1,
                        &quot;user_id&quot;: 3,
                        &quot;created_at&quot;: &quot;2022-04-06T17:56:17.000000Z&quot;,
                        &quot;updated_at&quot;: &quot;2022-04-06T17:56:17.000000Z&quot;
                    },
                    {
                        &quot;start&quot;: &quot;2022-04-08T16:04:43.000000Z&quot;,
                        &quot;end&quot;: &quot;2022-04-08T16:04:42.000000Z&quot;,
                        &quot;is_recurring&quot;: 1,
                        &quot;user_id&quot;: 3,
                        &quot;created_at&quot;: &quot;2022-04-06T17:56:17.000000Z&quot;,
                        &quot;updated_at&quot;: &quot;2022-04-06T17:56:17.000000Z&quot;
                    },
                    {
                        &quot;start&quot;: &quot;2022-04-15T16:04:43.000000Z&quot;,
                        &quot;end&quot;: &quot;2022-04-15T16:04:42.000000Z&quot;,
                        &quot;is_recurring&quot;: 1,
                        &quot;user_id&quot;: 3,
                        &quot;created_at&quot;: &quot;2022-04-06T17:56:17.000000Z&quot;,
                        &quot;updated_at&quot;: &quot;2022-04-06T17:56:17.000000Z&quot;
                    },
                    {
                        &quot;start&quot;: &quot;2022-04-22T16:04:43.000000Z&quot;,
                        &quot;end&quot;: &quot;2022-04-22T16:04:42.000000Z&quot;,
                        &quot;is_recurring&quot;: 1,
                        &quot;user_id&quot;: 3,
                        &quot;created_at&quot;: &quot;2022-04-06T17:56:17.000000Z&quot;,
                        &quot;updated_at&quot;: &quot;2022-04-06T17:56:17.000000Z&quot;
                    },
                    {
                        &quot;start&quot;: &quot;2022-04-29T16:04:43.000000Z&quot;,
                        &quot;end&quot;: &quot;2022-04-29T16:04:42.000000Z&quot;,
                        &quot;is_recurring&quot;: 1,
                        &quot;user_id&quot;: 3,
                        &quot;created_at&quot;: &quot;2022-04-06T17:56:17.000000Z&quot;,
                        &quot;updated_at&quot;: &quot;2022-04-06T17:56:17.000000Z&quot;
                    }
                ],
                &quot;created_at&quot;: &quot;2022-04-01T23:06:22.000000Z&quot;,
                &quot;updated_at&quot;: &quot;2022-04-01T23:06:22.000000Z&quot;
            }
        },
        {
            &quot;start&quot;: &quot;2022-04-22T16:04:43.000000Z&quot;,
            &quot;end&quot;: &quot;2022-04-22T16:04:42.000000Z&quot;,
            &quot;is_recurring&quot;: 1,
            &quot;user&quot;: {
                &quot;id&quot;: 3,
                &quot;name&quot;: &quot;Pera&quot;,
                &quot;email&quot;: &quot;pera@yopmail.com&quot;,
                &quot;timezone&quot;: &quot;Europe/Belgrade&quot;,
                &quot;availabilities&quot;: [
                    {
                        &quot;start&quot;: &quot;2022-04-01T17:04:42.000000Z&quot;,
                        &quot;end&quot;: &quot;2022-04-01T17:05:41.000000Z&quot;,
                        &quot;is_recurring&quot;: 0,
                        &quot;user_id&quot;: 3,
                        &quot;created_at&quot;: &quot;2022-04-05T20:56:41.000000Z&quot;,
                        &quot;updated_at&quot;: &quot;2022-04-05T20:56:41.000000Z&quot;
                    },
                    {
                        &quot;start&quot;: &quot;2022-04-01T17:01:42.000000Z&quot;,
                        &quot;end&quot;: &quot;2022-04-01T17:02:41.000000Z&quot;,
                        &quot;is_recurring&quot;: 0,
                        &quot;user_id&quot;: 3,
                        &quot;created_at&quot;: &quot;2022-04-05T20:56:41.000000Z&quot;,
                        &quot;updated_at&quot;: &quot;2022-04-05T20:56:41.000000Z&quot;
                    },
                    {
                        &quot;start&quot;: &quot;2022-04-01T17:04:42.000000Z&quot;,
                        &quot;end&quot;: &quot;2022-04-01T17:05:41.000000Z&quot;,
                        &quot;is_recurring&quot;: 0,
                        &quot;user_id&quot;: 3,
                        &quot;created_at&quot;: &quot;2022-04-05T20:56:41.000000Z&quot;,
                        &quot;updated_at&quot;: &quot;2022-04-05T20:56:41.000000Z&quot;
                    },
                    {
                        &quot;start&quot;: &quot;2022-04-01T17:04:42.000000Z&quot;,
                        &quot;end&quot;: &quot;2022-04-01T17:05:41.000000Z&quot;,
                        &quot;is_recurring&quot;: 0,
                        &quot;user_id&quot;: 3,
                        &quot;created_at&quot;: &quot;2022-04-05T20:56:41.000000Z&quot;,
                        &quot;updated_at&quot;: &quot;2022-04-05T20:56:41.000000Z&quot;
                    },
                    {
                        &quot;start&quot;: &quot;2022-04-01T16:04:43.000000Z&quot;,
                        &quot;end&quot;: &quot;2022-04-01T16:04:42.000000Z&quot;,
                        &quot;is_recurring&quot;: 1,
                        &quot;user_id&quot;: 3,
                        &quot;created_at&quot;: &quot;2022-04-06T17:56:17.000000Z&quot;,
                        &quot;updated_at&quot;: &quot;2022-04-06T17:56:17.000000Z&quot;
                    },
                    {
                        &quot;start&quot;: &quot;2022-04-08T16:04:43.000000Z&quot;,
                        &quot;end&quot;: &quot;2022-04-08T16:04:42.000000Z&quot;,
                        &quot;is_recurring&quot;: 1,
                        &quot;user_id&quot;: 3,
                        &quot;created_at&quot;: &quot;2022-04-06T17:56:17.000000Z&quot;,
                        &quot;updated_at&quot;: &quot;2022-04-06T17:56:17.000000Z&quot;
                    },
                    {
                        &quot;start&quot;: &quot;2022-04-15T16:04:43.000000Z&quot;,
                        &quot;end&quot;: &quot;2022-04-15T16:04:42.000000Z&quot;,
                        &quot;is_recurring&quot;: 1,
                        &quot;user_id&quot;: 3,
                        &quot;created_at&quot;: &quot;2022-04-06T17:56:17.000000Z&quot;,
                        &quot;updated_at&quot;: &quot;2022-04-06T17:56:17.000000Z&quot;
                    },
                    {
                        &quot;start&quot;: &quot;2022-04-22T16:04:43.000000Z&quot;,
                        &quot;end&quot;: &quot;2022-04-22T16:04:42.000000Z&quot;,
                        &quot;is_recurring&quot;: 1,
                        &quot;user_id&quot;: 3,
                        &quot;created_at&quot;: &quot;2022-04-06T17:56:17.000000Z&quot;,
                        &quot;updated_at&quot;: &quot;2022-04-06T17:56:17.000000Z&quot;
                    },
                    {
                        &quot;start&quot;: &quot;2022-04-29T16:04:43.000000Z&quot;,
                        &quot;end&quot;: &quot;2022-04-29T16:04:42.000000Z&quot;,
                        &quot;is_recurring&quot;: 1,
                        &quot;user_id&quot;: 3,
                        &quot;created_at&quot;: &quot;2022-04-06T17:56:17.000000Z&quot;,
                        &quot;updated_at&quot;: &quot;2022-04-06T17:56:17.000000Z&quot;
                    }
                ],
                &quot;created_at&quot;: &quot;2022-04-01T23:06:22.000000Z&quot;,
                &quot;updated_at&quot;: &quot;2022-04-01T23:06:22.000000Z&quot;
            }
        },
        {
            &quot;start&quot;: &quot;2022-04-29T16:04:43.000000Z&quot;,
            &quot;end&quot;: &quot;2022-04-29T16:04:42.000000Z&quot;,
            &quot;is_recurring&quot;: 1,
            &quot;user&quot;: {
                &quot;id&quot;: 3,
                &quot;name&quot;: &quot;Pera&quot;,
                &quot;email&quot;: &quot;pera@yopmail.com&quot;,
                &quot;timezone&quot;: &quot;Europe/Belgrade&quot;,
                &quot;availabilities&quot;: [
                    {
                        &quot;start&quot;: &quot;2022-04-01T17:04:42.000000Z&quot;,
                        &quot;end&quot;: &quot;2022-04-01T17:05:41.000000Z&quot;,
                        &quot;is_recurring&quot;: 0,
                        &quot;user_id&quot;: 3,
                        &quot;created_at&quot;: &quot;2022-04-05T20:56:41.000000Z&quot;,
                        &quot;updated_at&quot;: &quot;2022-04-05T20:56:41.000000Z&quot;
                    },
                    {
                        &quot;start&quot;: &quot;2022-04-01T17:01:42.000000Z&quot;,
                        &quot;end&quot;: &quot;2022-04-01T17:02:41.000000Z&quot;,
                        &quot;is_recurring&quot;: 0,
                        &quot;user_id&quot;: 3,
                        &quot;created_at&quot;: &quot;2022-04-05T20:56:41.000000Z&quot;,
                        &quot;updated_at&quot;: &quot;2022-04-05T20:56:41.000000Z&quot;
                    },
                    {
                        &quot;start&quot;: &quot;2022-04-01T17:04:42.000000Z&quot;,
                        &quot;end&quot;: &quot;2022-04-01T17:05:41.000000Z&quot;,
                        &quot;is_recurring&quot;: 0,
                        &quot;user_id&quot;: 3,
                        &quot;created_at&quot;: &quot;2022-04-05T20:56:41.000000Z&quot;,
                        &quot;updated_at&quot;: &quot;2022-04-05T20:56:41.000000Z&quot;
                    },
                    {
                        &quot;start&quot;: &quot;2022-04-01T17:04:42.000000Z&quot;,
                        &quot;end&quot;: &quot;2022-04-01T17:05:41.000000Z&quot;,
                        &quot;is_recurring&quot;: 0,
                        &quot;user_id&quot;: 3,
                        &quot;created_at&quot;: &quot;2022-04-05T20:56:41.000000Z&quot;,
                        &quot;updated_at&quot;: &quot;2022-04-05T20:56:41.000000Z&quot;
                    },
                    {
                        &quot;start&quot;: &quot;2022-04-01T16:04:43.000000Z&quot;,
                        &quot;end&quot;: &quot;2022-04-01T16:04:42.000000Z&quot;,
                        &quot;is_recurring&quot;: 1,
                        &quot;user_id&quot;: 3,
                        &quot;created_at&quot;: &quot;2022-04-06T17:56:17.000000Z&quot;,
                        &quot;updated_at&quot;: &quot;2022-04-06T17:56:17.000000Z&quot;
                    },
                    {
                        &quot;start&quot;: &quot;2022-04-08T16:04:43.000000Z&quot;,
                        &quot;end&quot;: &quot;2022-04-08T16:04:42.000000Z&quot;,
                        &quot;is_recurring&quot;: 1,
                        &quot;user_id&quot;: 3,
                        &quot;created_at&quot;: &quot;2022-04-06T17:56:17.000000Z&quot;,
                        &quot;updated_at&quot;: &quot;2022-04-06T17:56:17.000000Z&quot;
                    },
                    {
                        &quot;start&quot;: &quot;2022-04-15T16:04:43.000000Z&quot;,
                        &quot;end&quot;: &quot;2022-04-15T16:04:42.000000Z&quot;,
                        &quot;is_recurring&quot;: 1,
                        &quot;user_id&quot;: 3,
                        &quot;created_at&quot;: &quot;2022-04-06T17:56:17.000000Z&quot;,
                        &quot;updated_at&quot;: &quot;2022-04-06T17:56:17.000000Z&quot;
                    },
                    {
                        &quot;start&quot;: &quot;2022-04-22T16:04:43.000000Z&quot;,
                        &quot;end&quot;: &quot;2022-04-22T16:04:42.000000Z&quot;,
                        &quot;is_recurring&quot;: 1,
                        &quot;user_id&quot;: 3,
                        &quot;created_at&quot;: &quot;2022-04-06T17:56:17.000000Z&quot;,
                        &quot;updated_at&quot;: &quot;2022-04-06T17:56:17.000000Z&quot;
                    },
                    {
                        &quot;start&quot;: &quot;2022-04-29T16:04:43.000000Z&quot;,
                        &quot;end&quot;: &quot;2022-04-29T16:04:42.000000Z&quot;,
                        &quot;is_recurring&quot;: 1,
                        &quot;user_id&quot;: 3,
                        &quot;created_at&quot;: &quot;2022-04-06T17:56:17.000000Z&quot;,
                        &quot;updated_at&quot;: &quot;2022-04-06T17:56:17.000000Z&quot;
                    }
                ],
                &quot;created_at&quot;: &quot;2022-04-01T23:06:22.000000Z&quot;,
                &quot;updated_at&quot;: &quot;2022-04-01T23:06:22.000000Z&quot;
            }
        }
    ]
}</code>
 </pre>
    </span>
<span id="execution-results-GETapi-availabilities" hidden>
    <blockquote>Received response<span
                id="execution-response-status-GETapi-availabilities"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-availabilities"></code></pre>
</span>
<span id="execution-error-GETapi-availabilities" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-availabilities"></code></pre>
</span>
<form id="form-GETapi-availabilities" data-method="GET"
      data-path="api/availabilities"
      data-authed="1"
      data-hasfiles="0"
      data-isarraybody="0"
      data-headers='{"Authorization":"Bearer {YOUR_AUTH_KEY}","Content-Type":"application\/json","Accept":"application\/json"}'
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('GETapi-availabilities', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-GETapi-availabilities"
                    onclick="tryItOut('GETapi-availabilities');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-GETapi-availabilities"
                    onclick="cancelTryOut('GETapi-availabilities');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-GETapi-availabilities" hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-green">GET</small>
            <b><code>api/availabilities</code></b>
        </p>
                <p>
            <label id="auth-GETapi-availabilities" hidden>Authorization header:
                <b><code>Bearer </code></b><input type="text"
                                                                name="Authorization"
                                                                data-prefix="Bearer "
                                                                data-endpoint="GETapi-availabilities"
                                                                data-component="header"></label>
        </p>
                </form>

            <h2 id="endpoints-POSTapi-availabilities">Store availability timeframe.</h2>

<p>
<small class="badge badge-darkred">requires authentication</small>
</p>



<span id="example-requests-POSTapi-availabilities">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request POST \
    "availability-management.test/api/availabilities" \
    --header "Authorization: Bearer {YOUR_AUTH_KEY}" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json" \
    --data "{
    \"start\": \"2022-04-06 19:55:38\",
    \"end\": \"2022-04-06 19:55:38\",
    \"is_recurring\": \"odit\"
}"
</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "availability-management.test/api/availabilities"
);

const headers = {
    "Authorization": "Bearer {YOUR_AUTH_KEY}",
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "start": "2022-04-06 19:55:38",
    "end": "2022-04-06 19:55:38",
    "is_recurring": "odit"
};

fetch(url, {
    method: "POST",
    headers,
    body: JSON.stringify(body),
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-POSTapi-availabilities">
</span>
<span id="execution-results-POSTapi-availabilities" hidden>
    <blockquote>Received response<span
                id="execution-response-status-POSTapi-availabilities"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-availabilities"></code></pre>
</span>
<span id="execution-error-POSTapi-availabilities" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-availabilities"></code></pre>
</span>
<form id="form-POSTapi-availabilities" data-method="POST"
      data-path="api/availabilities"
      data-authed="1"
      data-hasfiles="0"
      data-isarraybody="0"
      data-headers='{"Authorization":"Bearer {YOUR_AUTH_KEY}","Content-Type":"application\/json","Accept":"application\/json"}'
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('POSTapi-availabilities', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-POSTapi-availabilities"
                    onclick="tryItOut('POSTapi-availabilities');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-POSTapi-availabilities"
                    onclick="cancelTryOut('POSTapi-availabilities');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-POSTapi-availabilities" hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-black">POST</small>
            <b><code>api/availabilities</code></b>
        </p>
                <p>
            <label id="auth-POSTapi-availabilities" hidden>Authorization header:
                <b><code>Bearer </code></b><input type="text"
                                                                name="Authorization"
                                                                data-prefix="Bearer "
                                                                data-endpoint="POSTapi-availabilities"
                                                                data-component="header"></label>
        </p>
                        <h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
        <p>
            <b><code>start</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
                <input type="text"
               name="start"
               data-endpoint="POSTapi-availabilities"
               value="2022-04-06 19:55:38"
               data-component="body" hidden>
    <br>
<p>Must be a valid date in the format <code>Y-m-d H:i:s</code>.</p>
        </p>
                <p>
            <b><code>end</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
                <input type="text"
               name="end"
               data-endpoint="POSTapi-availabilities"
               value="2022-04-06 19:55:38"
               data-component="body" hidden>
    <br>
<p>Must be a valid date in the format <code>Y-m-d H:i:s</code>.</p>
        </p>
                <p>
            <b><code>is_recurring</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
                <input type="text"
               name="is_recurring"
               data-endpoint="POSTapi-availabilities"
               value="odit"
               data-component="body" hidden>
    <br>

        </p>
        </form>

            <h2 id="endpoints-GETapi-users--id-">Display the specified resource.</h2>

<p>
</p>



<span id="example-requests-GETapi-users--id-">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request GET \
    --get "availability-management.test/api/users/14" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json"</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "availability-management.test/api/users/14"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers,
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-GETapi-users--id-">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <details class="annotation">
            <summary>
                <small onclick="textContent = parentElement.parentElement.open ? 'Show headers' : 'Hide headers'">Show headers</small>
            </summary>
            <pre><code class="language-http">cache-control: no-cache, private
content-type: application/json
x-ratelimit-limit: 60
x-ratelimit-remaining: 56
access-control-allow-origin: *
 </code></pre>
        </details>         <pre>

<code class="language-json">{
    &quot;data&quot;: {
        &quot;id&quot;: 3,
        &quot;name&quot;: &quot;Pera&quot;,
        &quot;email&quot;: &quot;pera@yopmail.com&quot;,
        &quot;timezone&quot;: &quot;Europe/Belgrade&quot;,
        &quot;availabilities&quot;: [
            {
                &quot;start&quot;: &quot;2022-04-01T17:04:42.000000Z&quot;,
                &quot;end&quot;: &quot;2022-04-01T17:05:41.000000Z&quot;,
                &quot;is_recurring&quot;: 0,
                &quot;user_id&quot;: 3,
                &quot;created_at&quot;: &quot;2022-04-05T20:56:41.000000Z&quot;,
                &quot;updated_at&quot;: &quot;2022-04-05T20:56:41.000000Z&quot;
            },
            {
                &quot;start&quot;: &quot;2022-04-01T17:01:42.000000Z&quot;,
                &quot;end&quot;: &quot;2022-04-01T17:02:41.000000Z&quot;,
                &quot;is_recurring&quot;: 0,
                &quot;user_id&quot;: 3,
                &quot;created_at&quot;: &quot;2022-04-05T20:56:41.000000Z&quot;,
                &quot;updated_at&quot;: &quot;2022-04-05T20:56:41.000000Z&quot;
            },
            {
                &quot;start&quot;: &quot;2022-04-01T17:04:42.000000Z&quot;,
                &quot;end&quot;: &quot;2022-04-01T17:05:41.000000Z&quot;,
                &quot;is_recurring&quot;: 0,
                &quot;user_id&quot;: 3,
                &quot;created_at&quot;: &quot;2022-04-05T20:56:41.000000Z&quot;,
                &quot;updated_at&quot;: &quot;2022-04-05T20:56:41.000000Z&quot;
            },
            {
                &quot;start&quot;: &quot;2022-04-01T17:04:42.000000Z&quot;,
                &quot;end&quot;: &quot;2022-04-01T17:05:41.000000Z&quot;,
                &quot;is_recurring&quot;: 0,
                &quot;user_id&quot;: 3,
                &quot;created_at&quot;: &quot;2022-04-05T20:56:41.000000Z&quot;,
                &quot;updated_at&quot;: &quot;2022-04-05T20:56:41.000000Z&quot;
            },
            {
                &quot;start&quot;: &quot;2022-04-01T16:04:43.000000Z&quot;,
                &quot;end&quot;: &quot;2022-04-01T16:04:42.000000Z&quot;,
                &quot;is_recurring&quot;: 1,
                &quot;user_id&quot;: 3,
                &quot;created_at&quot;: &quot;2022-04-06T17:56:17.000000Z&quot;,
                &quot;updated_at&quot;: &quot;2022-04-06T17:56:17.000000Z&quot;
            },
            {
                &quot;start&quot;: &quot;2022-04-08T16:04:43.000000Z&quot;,
                &quot;end&quot;: &quot;2022-04-08T16:04:42.000000Z&quot;,
                &quot;is_recurring&quot;: 1,
                &quot;user_id&quot;: 3,
                &quot;created_at&quot;: &quot;2022-04-06T17:56:17.000000Z&quot;,
                &quot;updated_at&quot;: &quot;2022-04-06T17:56:17.000000Z&quot;
            },
            {
                &quot;start&quot;: &quot;2022-04-15T16:04:43.000000Z&quot;,
                &quot;end&quot;: &quot;2022-04-15T16:04:42.000000Z&quot;,
                &quot;is_recurring&quot;: 1,
                &quot;user_id&quot;: 3,
                &quot;created_at&quot;: &quot;2022-04-06T17:56:17.000000Z&quot;,
                &quot;updated_at&quot;: &quot;2022-04-06T17:56:17.000000Z&quot;
            },
            {
                &quot;start&quot;: &quot;2022-04-22T16:04:43.000000Z&quot;,
                &quot;end&quot;: &quot;2022-04-22T16:04:42.000000Z&quot;,
                &quot;is_recurring&quot;: 1,
                &quot;user_id&quot;: 3,
                &quot;created_at&quot;: &quot;2022-04-06T17:56:17.000000Z&quot;,
                &quot;updated_at&quot;: &quot;2022-04-06T17:56:17.000000Z&quot;
            },
            {
                &quot;start&quot;: &quot;2022-04-29T16:04:43.000000Z&quot;,
                &quot;end&quot;: &quot;2022-04-29T16:04:42.000000Z&quot;,
                &quot;is_recurring&quot;: 1,
                &quot;user_id&quot;: 3,
                &quot;created_at&quot;: &quot;2022-04-06T17:56:17.000000Z&quot;,
                &quot;updated_at&quot;: &quot;2022-04-06T17:56:17.000000Z&quot;
            }
        ],
        &quot;created_at&quot;: &quot;2022-04-01T23:06:22.000000Z&quot;,
        &quot;updated_at&quot;: &quot;2022-04-01T23:06:22.000000Z&quot;
    }
}</code>
 </pre>
    </span>
<span id="execution-results-GETapi-users--id-" hidden>
    <blockquote>Received response<span
                id="execution-response-status-GETapi-users--id-"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-users--id-"></code></pre>
</span>
<span id="execution-error-GETapi-users--id-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-users--id-"></code></pre>
</span>
<form id="form-GETapi-users--id-" data-method="GET"
      data-path="api/users/{id}"
      data-authed="0"
      data-hasfiles="0"
      data-isarraybody="0"
      data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}'
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('GETapi-users--id-', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-GETapi-users--id-"
                    onclick="tryItOut('GETapi-users--id-');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-GETapi-users--id-"
                    onclick="cancelTryOut('GETapi-users--id-');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-GETapi-users--id-" hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-green">GET</small>
            <b><code>api/users/{id}</code></b>
        </p>
                    <h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
                    <p>
                <b><code>id</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
                <input type="number"
               name="id"
               data-endpoint="GETapi-users--id-"
               value="14"
               data-component="url" hidden>
    <br>
<p>The ID of the user.</p>
            </p>
                    </form>

    

        
    </div>
    <div class="dark-box">
                    <div class="lang-selector">
                                                        <button type="button" class="lang-button" data-language-name="bash">bash</button>
                                                        <button type="button" class="lang-button" data-language-name="javascript">javascript</button>
                            </div>
            </div>
</div>
</body>
</html>
