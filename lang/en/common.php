<?php

return [
    'verification_success' => "Successful verification.",
    'successful_login' => 'Successful login.',
    'unsuccessful_login' => 'Unsuccessful login, check your email or password and try again.',
    'successful_logout' => 'Successful logout.',
    'verification_link' => 'Verification link sent.',
];
