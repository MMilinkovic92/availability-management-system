<?php

namespace App\Http\Controllers;

use App\Http\Requests\EmailVerificationRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;

class UserVerifyController extends Controller
{
    /**
     * Verify registration using email.
     *
     * @authenticated
     * @responseFile storage/responses/users/verify-email.json
     *
     * @param EmailVerificationRequest $request
     * @return JsonResponse
     */
    public function index(EmailVerificationRequest $request): JsonResponse
    {
        $request->fulfill();

        return response()->json(['message' => Lang::get('common.verification_success')]);
    }

    /**
     * Verify notification, sends email with verification link.
     *
     * @authenticated
     * @responseFile storage/responses/users/verify-notification.json
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request)
    {
        $request->user()->sendEmailVerificationNotification();

        return response()->json(['message' => Lang::get('common.verification_link')]);
    }
}
