<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserLoginRequest;
use App\Http\Requests\UserLogoutRequest;
use App\Http\Resources\UserResource;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;

class AuthenticationController extends Controller
{
    /**
     * Login user.
     *
     * @responseFile storage/responses/users/login.json
     *
     * @param UserLoginRequest $loginRequest
     * @return JsonResponse
     */
    public function store(UserLoginRequest $loginRequest): JsonResponse
    {
        if (Auth::attempt($loginRequest->only(['email', 'password']))) {
            $user = User::where('email', $loginRequest->get('email'))->first();
            $token = $user->createToken($user->email);

            $user->changeTimezone();

            return response()->json([
                'message' => Lang::get('common.successful_login'),
                'user' => new UserResource($user),
                'token' => $token->plainTextToken
            ]);
        }

        return response()->json(['message' => Lang::get('common.unsuccessful_login')], 422);
    }

    /**
     * Logout user.
     *
     * @responseFile storage/responses/users/logout.json
     *
     * @param UserLogoutRequest $userLogoutRequest
     * @return JsonResponse
     */
    public function destroy(UserLogoutRequest $userLogoutRequest)
    {
        $userLogoutRequest->user()->tokens()->delete();

        return response()->json(['message' => Lang::get('common.successful_logout')]);
    }
}
