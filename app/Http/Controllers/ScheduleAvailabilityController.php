<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreAvailabilityRequest;
use App\Http\Resources\ScheduleAvailabilityResource;
use App\Models\ScheduleAvailability;
use App\Services\Interfaces\AvailabilityManagementSystemServiceInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

class ScheduleAvailabilityController extends Controller
{
    protected AvailabilityManagementSystemServiceInterface $availabilityManagementSystemService;

    /**
     * @param AvailabilityManagementSystemServiceInterface $availabilityManagementSystemService
     */
    public function __construct(AvailabilityManagementSystemServiceInterface $availabilityManagementSystemService)
    {
        $this->availabilityManagementSystemService = $availabilityManagementSystemService;
    }

    /**
     * Return all availabilities.
     *
     * @authenticated
     * @responseFile storage/responses/availabilities/availabilities.index.json
     *
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        if (!Gate::allows('list-availabilities')) {
            return response()->json(['message' => 'Not authorized']);
        }

        $availabilities = ScheduleAvailability::filter(request()->all())->get();

        return response()->json(['availabilities' => ScheduleAvailabilityResource::collection($availabilities)]);
    }

    /**
     * Store availability timeframe.
     *
     * @authenticated
     * @responseFile storage/responses/availabilities/availabilities.store.json
     *
     * @param StoreAvailabilityRequest $storeAvailabilityRequest
     * @return JsonResponse
     */
    public function store(StoreAvailabilityRequest $storeAvailabilityRequest): JsonResponse
    {
        if (!Gate::allows('set-availability', Auth::user())) {
            return response()->json(['message' => 'Not authorized']);
        }

        $user =  $storeAvailabilityRequest->user();

        $start = $storeAvailabilityRequest->start;
        $end = $storeAvailabilityRequest->end;
        $isRecurring = $storeAvailabilityRequest->is_recurring;
        $weeks = $storeAvailabilityRequest->weeks;

        if (!$user->canSchedule($start, $end, $isRecurring, $weeks)) {
            return response()->json(['message' => 'Invalid date range for availability schedule.'], 422);
        }

        $this->availabilityManagementSystemService
            ->setUser($user)
            ->setStart($storeAvailabilityRequest->start)
            ->setEnd($storeAvailabilityRequest->end)
            ->setRecurring($storeAvailabilityRequest->is_recurring)
            ->setWeeks($storeAvailabilityRequest->weeks)
            ->schedule();

        return response()->json(['message' => 'Availability scheduled.']);
    }
}
