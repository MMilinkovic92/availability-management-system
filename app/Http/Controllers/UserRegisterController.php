<?php

namespace App\Http\Controllers;

use App\Http\Requests\RegisterUserRequest;
use App\Http\Resources\UserResource;
use App\Models\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Hash;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;

class UserRegisterController extends Controller
{

    /**
     * Register User.
     *
     * @responseFile storage/responses/users/store.json
     *
     * @param RegisterUserRequest $registerUserRequest
     * @return JsonResponse
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(RegisterUserRequest $registerUserRequest): JsonResponse
    {
        $data = $registerUserRequest->all();
        $data['password'] = Hash::make(request()->get('password'));

        $user = User::create($data);

        event(new Registered($user));

        return response()->json((new UserResource($user)));
    }
}
