<?php

namespace App\Providers;

use App\Services\Interfaces\AvailabilityManagementSystemServiceInterface;
use App\Services\SystemManagement\AvailabilityManagementSystemService;
use App\Services\Timezone\DateTimezoneService;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * All of the container bindings that should be registered.
     *
     * @var array
     */
    public $bindings = [
        AvailabilityManagementSystemServiceInterface::class => AvailabilityManagementSystemService::class,
    ];

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind('DateTimezoneService',function(){
            return new DateTimezoneService();
        });
    }
}
