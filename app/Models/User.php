<?php

namespace App\Models;

use App\Facades\TimezoneFacade;
use Carbon\Carbon;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable implements MustVerifyEmail
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'timezone',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Get all user scheduled availabilities.
     *
     * @return HasMany
     */
    public function availabilities(): HasMany
    {
        return $this->hasMany(ScheduleAvailability::class);
    }

    /**
     * Change global timezone for authenticated user.
     *
     * @return void
     */
    public function changeTimezone(): void
    {
        config(['app.timezone' => $this->timezone]);
    }

    /**
     * Check can schedule availability.
     *
     * @param string $start
     * @param string $end
     * @param bool $isRecurring
     * @param int $weeks
     * @return bool
     */
    public function canSchedule(string $start, string $end, bool $isRecurring = false, int $weeks = 0): bool
    {
        //Probaj da implementiras chain of responsibility ako ima smisla.
        $start = Carbon::createFromFormat('Y-m-d H:i:s', $start);
        $end = Carbon::createFromFormat('Y-m-d H:i:s', $end);

        if ($isRecurring) {
            for ($i = 0; $i < $weeks; $i++) {
                if (!$this->checkAvailableDates($start->toDateTimeString(), $end->toDateTimeString())) {
                    return false;
                }

                $start->addWeek();
                $end->addWeek();
            }

            return true;
        }

        return $this->checkAvailableDates($start->toDateTimeString(), $end->toDateTimeString());
    }

    /**
     * Check is available timeframe with start and end dates.
     *
     * @param string $start
     * @param string $end
     * @return bool
     */
    private function checkAvailableDates(string $start, string $end): bool
    {
        $start = TimezoneFacade::convertToUTC($start);
        $end = TimezoneFacade::convertToUTC($end);

        return  !($this->availabilities()->whereBetween('start', [$start, $end])->exists() ||
                  $this->availabilities()->whereBetween('end', [$start, $end])->exists() ||
                  $this->availabilities()->where('start', '<', $start)->where('end', '>', $start)->exists() ||
                  $this->availabilities()->where('start', $start)->where('end', $end)->exists());
    }

    /**
     * Check user has candidate role.
     *
     * @return bool
     */
    public function isCandidate(): bool
    {
        return !$this->isAdmin();
    }

    /**
     * Check user has admin role.
     *
     * @return bool
     */
    public function isAdmin(): bool
    {
        return $this->is_admin;
    }
}
