<?php

namespace App\Models;

use App\Facades\TimezoneFacade;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class ScheduleAvailability extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'start',
        'end',
        'is_recurring',
        'user_id',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'id',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'start' => 'datetime:Y-m-d h:i:s',
        'end' => 'datetime:Y-m-d h:i:s',
    ];

    /**
     * Get user that owns this model.
     *
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Mutator for start date. Timezone awareness.
     *
     *
     * @param $value
     * @return Carbon
     */
    public function getStartAttribute($value): Carbon
    {
        return TimezoneFacade::convertToLocalTimezone($value);
    }

    /**
     * Mutator for end date. Timezone awareness.
     *
     * @param $value
     * @return Carbon
     */
    public function getEndAttribute($value): Carbon
    {
        return TimezoneFacade::convertToLocalTimezone($value);
    }

    /**
     * Mutator for start date. Timezone awareness.
     *
     *
     * @param $value
     * @return void
     */
    public function setStartAttribute($value): void
    {
        $this->attributes['start'] = TimezoneFacade::convertToUTC($value);
    }

    /**
     * Mutator for end date. Timezone awareness.
     *
     * @param $value
     * @return void
     */
    public function setEndAttribute($value): void
    {
        $this->attributes['end'] = TimezoneFacade::convertToUTC($value);
    }

    /**
     * Filter availabilities by provided values.
     *
     * @param Builder $query
     * @param array $data
     * @return Builder
     */
    public function scopeFilter(Builder $query, array $data): Builder
    {
        if (array_key_exists('user_id', $data)) {
            $query->where('user_id', $data['user_id']);
        }

        if (array_key_exists('start', $data)) {
            $query->where('start', '>=', $data['start']);
        }

        if (array_key_exists('end', $data)) {
            $query->where('end', '<=', $data['end']);
        }

        return $query;
    }
}
