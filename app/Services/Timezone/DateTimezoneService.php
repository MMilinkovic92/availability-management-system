<?php

namespace App\Services\Timezone;

use Carbon\Carbon;

class DateTimezoneService
{
    /**
     * Convert any date in datetime format ('Y-m-d H:i:s') from application timezone to UTC.
     *
     * @param string $date
     * @return string
     */
    public function convertToUTC(string $date): string
    {
        return Carbon::
                createFromFormat('Y-m-d H:i:s', $date, config('app.timezone'))
                    ->setTimezone('UTC')->toDateTimeString();
    }

    /**
     * Convert from UTC (default database) to local timezone.
     *
     * @param string $date
     * @return Carbon
     */
    public function convertToLocalTimezone(string $date): Carbon
    {
        return Carbon::
                createFromFormat('Y-m-d H:i:s', $date, 'UTC')
                ->setTimezone(config('app.timezone'));
    }
}
