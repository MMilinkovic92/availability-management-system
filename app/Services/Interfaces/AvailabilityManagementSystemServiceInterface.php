<?php

namespace App\Services\Interfaces;

interface AvailabilityManagementSystemServiceInterface
{
    public function schedule(): void;
}
