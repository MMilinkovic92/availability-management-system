<?php

namespace App\Services\SystemManagement;

use App\Models\ScheduleAvailability;
use App\Models\User;
use App\Services\Interfaces\AvailabilityManagementSystemServiceInterface;
use Carbon\Carbon;

class AvailabilityManagementSystemService implements AvailabilityManagementSystemServiceInterface
{
    public string $start;
    public string $end;
    public User $user;
    public bool $isRecurring = false;
    public int $weeks = 0;

    public function schedule(): void
    {
        $start = Carbon::createFromFormat('Y-m-d H:i:s', $this->start);
        $end = Carbon::createFromFormat('Y-m-d H:i:s', $this->end);

        if ($this->isRecurring) {
            for ($i = 0; $i < $this->weeks; $i++) {
                ScheduleAvailability::create([
                    'start' => $start->toDateTimeString(),
                    'end' => $end->toDateTimeString(),
                    'is_recurring' => $this->isRecurring,
                    'user_id' => $this->user->id
                ]);

                $start->addWeek();
                $end->addWeek();
            }
        } else {
            ScheduleAvailability::create([
                'start' => $start->toDateTimeString(),
                'end' => $end->toDateTimeString(),
                'is_recurring' => $this->isRecurring,
                'user_id' => $this->user->id
            ]);
        }
    }

    /**
     * Set start date;
     *
     * @param string $start
     * @return $this
     */
    public function setStart(string $start): self
    {
        $this->start = $start;
        return $this;
    }

    /**
     * Set end date.
     *
     * @param string $end
     * @return $this
     */
    public function setEnd(string $end): self
    {
        $this->end = $end;
        return $this;
    }

    /**
     * Set user.
     *
     * @param User $user
     * @return $this
     */
    public function setUser(User $user): self
    {
        $this->user = $user;
        return  $this;
    }

    /**
     * Set recurring availability.
     *
     * @param bool $isRecurring
     * @return $this
     */
    public function setRecurring(bool $isRecurring): self
    {
        $this->isRecurring = $isRecurring;
        return $this;
    }

    /**
     * Set weeks for recurring.
     *
     * @param int $weeks
     * @return $this
     */
    public function setWeeks(int $weeks): self
    {
        $this->weeks = $weeks;
        return $this;
    }
}
