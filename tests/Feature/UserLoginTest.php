<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Lang;
use Tests\TestCase;

class UserLoginTest extends TestCase
{
    protected const DEFAULT_PASSWORD = "default_password";
    protected const DEFAULT_EMAIL = "pera@yopmail.com";

    /**
     * @test
     * @dataProvider invalidUserCredentialsDataProvider
     * @return void
     */
    public function user_failed_to_login($email, $password, $errorMessage)
    {
        $response = $this->postJson(route('login'), [
            'email' => $email,
            'password' => $password
        ]);

        $response
            ->assertStatus(422)
            ->assertJsonPath('message', $errorMessage);
    }

    /**
     * @test
     *
     * @return void
     */
    public function user_successful_login()
    {
        $user = User::factory()->create(['password' => Hash::make(self::DEFAULT_PASSWORD)]);
        $response = $this->postJson(route('login'), [
           'email' => $user->email,
           'password' => self::DEFAULT_PASSWORD
        ]);

        $response
            ->assertStatus(200)
            ->assertJsonPath('message', Lang::get('common.successful_login'))
            ->assertJsonPath('user.email', $user->email);
    }

    /**
     * Test cases for validation rules in user authentication process.
     *
     * @return string[][]
     */
    public function invalidUserCredentialsDataProvider(): array
    {
        return [
            'invalid_email' => ['invalid email', self::DEFAULT_PASSWORD, "The email must be a valid email address."],
            'invalid_password' => ['pera1234@yopmail.com', 'short', "The password must be at least 6 characters."]
        ];
    }
}
