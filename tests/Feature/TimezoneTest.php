<?php

namespace Tests\Feature;

use App\Models\ScheduleAvailability;
use Carbon\Carbon;
use Tests\TestCase;

class TimezoneTest extends TestCase
{
    /**
     * @test
     *
     * Test timezone read from database. Database has UTC time, read depends on configuration.
     *
     * @return void
     */
    public function database_timezone_should_be_default_but_user_should_always_see_his_timezone()
    {
        $response = $this->loginUser('Europe/Athens');
        $user = $response->user;
        $token = $response->token;

        $start = Carbon::now()->timezone($user->timezone)->toDateTimeString();
        $end = Carbon::now()->addMinutes(60)->timezone($user->timezone)->toDateTimeString();

        $response = $this
            ->withHeaders(['Authorization'=>'Bearer '. $token])
            ->postJson(route('availabilities.store'), [
                'start' => $start,
                'end' => $end,
                'is_recurring' => false,
                'weeks' => 0,
            ]);

        $response
            ->assertStatus(200)
            ->assertJsonPath('message', "Availability scheduled.");

        $this->assertDatabaseHas('schedule_availabilities', [
            'start' => Carbon::createFromFormat('Y-m-d H:i:s', $start, $user->timezone)->setTimezone('UTC')->toDateTimeString(),
            'end' => Carbon::createFromFormat('Y-m-d H:i:s', $end, $user->timezone)->setTimezone('UTC')->toDateTimeString(),
        ]);

        $this->assertEquals(ScheduleAvailability::where('user_id', $user->id)->first()->start, $start);
        $this->assertEquals(ScheduleAvailability::where('user_id', $user->id)->first()->end, $end);
    }
}
