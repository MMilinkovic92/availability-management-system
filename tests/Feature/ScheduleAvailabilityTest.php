<?php

namespace Tests\Feature;

use App\Models\ScheduleAvailability;
use Carbon\Carbon;
use Tests\TestCase;

class ScheduleAvailabilityTest extends TestCase
{
    /**
     * @test
     *
     * @dataProvider dateScheduleDataProvider
     * @return void
     */
    public function successful_schedule($start, $end, $isRecurring, $weeks)
    {
        $response = $this->loginUser();
        $user = $response->user;
        $token = $response->token;

        $response = $this
                    ->withHeaders(['Authorization'=>'Bearer '. $token])
                    ->postJson(route('availabilities.store'), [
                        'start' => $start,
                        'end' => $end,
                        'is_recurring' => $isRecurring,
                        'weeks' => $weeks,
                    ]);

        $response
            ->assertStatus(200)
            ->assertJsonPath('message', "Availability scheduled.");

        $this->assertDatabaseHas('schedule_availabilities', [
            'user_id' => $user->id,
        ]);
    }

    /**
     * @test
     *
     * @dataProvider dateScheduleDataProvider
     * @return void
     */
    public function unsuccessful_schedule($start, $end, $isRecurring, $weeks)
    {
        $response = $this->loginUser();
        $user = $response->user;
        $token = $response->token;

        $response = $this
            ->withHeaders(['Authorization'=>'Bearer '. $token])
            ->postJson(route('availabilities.store'), [
                'start' => $start,
                'end' => $end,
                'is_recurring' => $isRecurring,
                'weeks' => $weeks,
            ]);

        $response
            ->assertStatus(200)
            ->assertJsonPath('message', "Availability scheduled.");

        $this->assertDatabaseHas('schedule_availabilities', [
            'user_id' => $user->id,
        ]);

        $response = $this
            ->withHeaders(['Authorization'=>'Bearer '. $token])
            ->postJson(route('availabilities.store'), [
                'start' => $start,
                'end' => $end,
                'is_recurring' => $isRecurring,
                'weeks' => $weeks,
            ]);

        $response
            ->assertStatus(422)
            ->assertJsonPath('message', "Invalid date range for availability schedule.");
    }

    /**
     * @test
     *
     * Try to add availability with start date in already existing timeframe but end date is valid.
     *
     * @dataProvider dateScheduleDataProvider
     * @return void
     */
    public function unsuccessful_schedule_start_date_in_existing_timeframe($start, $end, $isRecurring, $weeks)
    {
        $response = $this->loginUser();
        $user = $response->user;
        $token = $response->token;

        $response = $this
            ->withHeaders(['Authorization'=>'Bearer '. $token])
            ->postJson(route('availabilities.store'), [
                'start' => $start,
                'end' => $end,
                'is_recurring' => $isRecurring,
                'weeks' => $weeks,
            ]);

        $response
            ->assertStatus(200)
            ->assertJsonPath('message', "Availability scheduled.");

        $this->assertDatabaseHas('schedule_availabilities', [
            'user_id' => $user->id,
        ]);

        $end = Carbon::createFromFormat('Y-m-d H:i:s', $end)->timezone("UTC")->addHour(5)->toDateTimeString();

        $response = $this
            ->withHeaders(['Authorization'=>'Bearer '. $token])
            ->postJson(route('availabilities.store'), [
                'start' => $start,
                'end' => $end,
                'is_recurring' => $isRecurring,
                'weeks' => $weeks,
            ]);

        $response
            ->assertStatus(422)
            ->assertJsonPath('message', "Invalid date range for availability schedule.");
    }

    /**
     * @test
     *
     * Try to add availability with end date of existing timeframe is in current timeframe range.
     *
     * @dataProvider dateScheduleDataProvider
     * @return void
     */
    public function unsuccessful_schedule_end_date_from_existing_timeframe_is_in_current_timeframe_range($start, $end, $isRecurring, $weeks)
    {
        $response = $this->loginUser();
        $user = $response->user;
        $token = $response->token;

        $response = $this
            ->withHeaders(['Authorization'=>'Bearer '. $token])
            ->postJson(route('availabilities.store'), [
                'start' => $start,
                'end' => $end,
                'is_recurring' => $isRecurring,
                'weeks' => $weeks,
            ]);

        $response
            ->assertStatus(200)
            ->assertJsonPath('message', "Availability scheduled.");

        $this->assertDatabaseHas('schedule_availabilities', [
            'user_id' => $user->id,
        ]);
        $start = $end;
        $end = Carbon::createFromFormat('Y-m-d H:i:s', $end)->timezone("UTC")->addHour(5)->toDateTimeString();

        $response = $this
            ->withHeaders(['Authorization'=>'Bearer '. $token])
            ->postJson(route('availabilities.store'), [
                'start' => $start,
                'end' => $end,
                'is_recurring' => $isRecurring,
                'weeks' => $weeks,
            ]);

        $response
            ->assertStatus(422)
            ->assertJsonPath('message', "Invalid date range for availability schedule.");
    }

    /**
     * @test
     *
     * Try to add availability with end date of existing timeframe is in current timeframe range.
     *
     * @dataProvider dateScheduleDataProvider
     * @return void
     */
    public function unsuccessful_schedule_complete_current_iframe_is_in_existing_timeframe($start, $end, $isRecurring, $weeks)
    {
        $response = $this->loginUser();
        $user = $response->user;
        $token = $response->token;

        $response = $this
            ->withHeaders(['Authorization'=>'Bearer '. $token])
            ->postJson(route('availabilities.store'), [
                'start' => $start,
                'end' => $end,
                'is_recurring' => $isRecurring,
                'weeks' => $weeks,
            ]);

        $response
            ->assertStatus(200)
            ->assertJsonPath('message', "Availability scheduled.");

        $this->assertDatabaseHas('schedule_availabilities', [
            'user_id' => $user->id,
        ]);

        $start = Carbon::createFromFormat('Y-m-d H:i:s', $start)->timezone("UTC")->addMinutes(5)->toDateTimeString();
        $end = Carbon::createFromFormat('Y-m-d H:i:s', $end)->timezone("UTC")->subMinute(5)->toDateTimeString();

        $response = $this
            ->withHeaders(['Authorization'=>'Bearer '. $token])
            ->postJson(route('availabilities.store'), [
                'start' => $start,
                'end' => $end,
                'is_recurring' => $isRecurring,
                'weeks' => $weeks,
            ]);

        $response
            ->assertStatus(422)
            ->assertJsonPath('message', "Invalid date range for availability schedule.");
    }

    /**
     * @test
     *
     * Try to add availability with timeframe that already exist with same start && end date.
     *
     * @dataProvider dateScheduleDataProvider
     * @return void
     */
    public function unsuccessful_schedule_complete_current_iframe_is_same_as_existing_timeframe($start, $end, $isRecurring, $weeks)
    {
        $response = $this->loginUser();
        $user = $response->user;
        $token = $response->token;

        $response = $this
            ->withHeaders(['Authorization'=>'Bearer '. $token])
            ->postJson(route('availabilities.store'), [
                'start' => $start,
                'end' => $end,
                'is_recurring' => $isRecurring,
                'weeks' => $weeks,
            ]);

        $response
            ->assertStatus(200)
            ->assertJsonPath('message', "Availability scheduled.");

        $this->assertDatabaseHas('schedule_availabilities', [
            'user_id' => $user->id,
        ]);

        $start = Carbon::createFromFormat('Y-m-d H:i:s', $start)->timezone("UTC")->addMinutes(5)->toDateTimeString();
        $end = Carbon::createFromFormat('Y-m-d H:i:s', $end)->timezone("UTC")->subMinute(5)->toDateTimeString();

        $response = $this
            ->withHeaders(['Authorization'=>'Bearer '. $token])
            ->postJson(route('availabilities.store'), [
                'start' => $start,
                'end' => $end,
                'is_recurring' => $isRecurring,
                'weeks' => $weeks,
            ]);

        $response
            ->assertStatus(422)
            ->assertJsonPath('message', "Invalid date range for availability schedule.");
    }

    /**
     * Simple dates created for test purpose.
     *
     * @return array[]
     */
    public function dateScheduleDataProvider(): array
    {
        $start = Carbon::now()->timezone("UTC")->toDateTimeString();
        $end = Carbon::now()->addMinutes(60)->timezone("UTC")->toDateTimeString();

        return [
            'simple_date_range' => [$start, $end, false, 5],
        ];
    }
}
