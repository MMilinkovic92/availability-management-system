<?php

namespace Tests\Feature;

use Tests\TestCase;

class UserRegisterTest extends TestCase
{
    /**
     * @test
     * @dataProvider userDataProvider
     * @return void
     */
    public function user_register($name, $email, $password, $timezone)
    {
        $response = $this->postJson(route('register'), [
            'name' => $name,
            'email' => $email,
            'password' => $password,
            'timezone' => $timezone,
        ]);

        $response
            ->assertStatus(200)
            ->assertJsonPath('name', $name)
            ->assertJsonPath('email', $email)
            ->assertJsonPath('timezone', $timezone);

        $this->assertDatabaseHas('users', [
            'name' => $name,
            'email' => $email,
            'timezone' => $timezone
        ]);
    }

    /**
     * @test
     * @dataProvider invalidUserDataProvider
     * @return void
     */
    public function user_validation($name, $email, $password, $timezone, $expectedMessage)
    {
        $response = $this->postJson(route('register'), [
            'name' => $name,
            'email' => $email,
            'password' => $password,
            'timezone' => $timezone
        ]);

        $response
            ->assertStatus(422)
            ->assertJsonPath('message', $expectedMessage);

        if (!$name && !$email && !$password && !$timezone) {
            $this->assertDatabaseMissing('users', [
                'email' => $email,
            ]);
        }
    }

    /**
     * Test cases for successful user registration.
     *
     * @return string[][]
     */
    public function userDataProvider(): array
    {
        return [
            'first_user' => ['Test Pera', 'pera@yopmail.com', 'pera123', 'Europe/Belgrade'],
            'second_user' => ['Test Mika', 'mika@yopmail.com', 'mika123', 'Europe/Athens'],
            'third_user' => ['Test Zika', 'zika@yopmail.com', 'zika123', 'America/New_York'],
        ];
    }

    /**
     * Test cases for validation rules in user registration process.
     *
     * @return string[][]
     */
    public function invalidUserDataProvider(): array
    {
        return [
            'empty_fields' => ['', '', '', '', 'The name field is required. (and 3 more errors)'],
            'wrong_email' => ['Pera', 'wrong email', 'pera123', 'Europe/Belgrade', 'The email must be a valid email address.'],
            'wrong_password' => ['Pera', 'pera@yopmail.com', 'bad', 'Europe/Belgrade', 'The password must be at least 7 characters.']
        ];
    }
}
