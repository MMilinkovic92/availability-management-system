<?php

namespace Tests\Feature;

use App\Models\ScheduleAvailability;
use Tests\TestCase;

class GetAllAvailabilitiesTest extends TestCase
{
    /**
     * @test
     * @return void
     */
    public function all_availabilities()
    {
        $token = $this->loginUser()->token;
        $this->createAvailabilities();

        $response = $this->withHeaders(['Authorization'=>'Bearer '. $token])->getJson(route('availabilities.index'));

        $response->assertStatus(200);
    }

    /**
     * Create list of availabilities.
     *
     * @return void
     */
    private function createAvailabilities()
    {
        ScheduleAvailability::factory(10)->create();
    }
}
