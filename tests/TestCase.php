<?php

namespace Tests;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Illuminate\Support\Facades\Hash;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication, RefreshDatabase;

    /**
     * Login user to get Bearer token.
     *
     * @param string $timezone
     * @return object
     */
    protected function loginUser(string $timezone = "UTC"): object
    {
        $user = User::factory()->create([
            'password' => Hash::make("somepassword"),
            'timezone' => $timezone
        ]);

        $response = $this->postJson(route('login'), [
            'email' => $user->email,
            'password' => "somepassword"
        ]);

        return json_decode($response->getContent());
    }
}
