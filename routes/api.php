<?php

use App\Http\Controllers\AuthenticationController;
use App\Http\Controllers\ScheduleAvailabilityController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\UserRegisterController;
use App\Http\Controllers\UserVerifyController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('/register', UserRegisterController::class)->name('register');
Route::post('/login', [AuthenticationController::class, 'store'])->name('login');
Route::middleware('auth:sanctum')->delete('/logout', [AuthenticationController::class, 'destroy'])->name('logout');
Route::middleware('auth:sanctum')->get('/email/verify/{id}/{hash}', [UserVerifyController::class, 'index'])->name('verification.verify');
Route::middleware('auth:sanctum')->post('/email/verification-notification', [UserVerifyController::class, 'store'])->name('verification.send');

Route::middleware('auth:sanctum')->resource('availabilities', ScheduleAvailabilityController::class)->only(['index', 'store']);
Route::middleware('auth:sanctum')->resource('users', UserController::class)->only(['show']);
